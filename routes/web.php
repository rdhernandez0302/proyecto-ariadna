<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

Route::get('/', function () {
    return redirect('login');
});

Auth::routes(['register' => false]);

Route::middleware(['checkPermissions'])->group(function() {

    /**
     * Ruta de radicados
     *
     */
    Route::resource('documents', 'Documents\DocumentsController', [
        'except' => [ 'edit', 'update', 'create' ]
    ]);

    /**
     * Ruta de temas
     *
     */
    Route::post('themes/list', 'Themes\ThemesController@list')->name('themes.list');
    Route::resource('themes', 'Themes\ThemesController', [
        'except' => [ 'destroy', 'edit', 'create' ]
    ]);

    Route::get('home', 'Home\HomeController@index')->name('home.index');

    /**
     *
     * Ruta de roles
     *
     */
    Route::post('roles/list', 'Roles\RolesController@list')->name('roles.list');
    Route::put('roles/update_permission_menu/{id}',  'Roles\RolesController@permission_menu')->name('roles.permission_menu');
    Route::put('roles/update_permission/{id}',  'Roles\RolesController@permission')->name('roles.permission');
    Route::resource('roles', 'Roles\RolesController', [
        'except' => [ 'destroy', 'edit', 'create' ]
    ]);

    /**
     * Ruta de menus
     *
     */
    Route::post('menus/list', 'Menus\MenusController@list')->name('menus.list');
    Route::resource('menus', 'Menus\MenusController', [
        'except' => [ 'destroy', 'edit', 'create' ]
    ]);

    /**
     * Ruta de permisos
     *
     */
    Route::post('permissions/list', 'Permissions\PermissionsController@list')->name('permissions.list');
    Route::resource('permissions', 'Permissions\PermissionsController', [
        'except' => [ 'destroy', 'edit', 'create' ]
    ]);

    /**
     * Ruta de usuarios registrado
     *
     */
    Route::post('/users/list', 'Users\UsersController@list')->name('users.list');
    Route::post('/users/profile_update', 'Users\UsersController@profile_update')->name('users.profile_update');
    Route::post('/users/profile_password', 'Users\UsersController@profile_password')->name('users.profile_password');
    Route::post('/users/{id}', 'Users\UsersController@update')->name('users.update');
    Route::get('/users/change_status/{id}', 'Users\UsersController@change_status')->name('users.change_status');
    Route::post('/users/change_password/{id}', 'Users\UsersController@change_password')->name('users.change_password');
    Route::get('profile', 'Users\UsersController@profile')->name('users.profile');
    Route::resource('/users', 'Users\UsersController', [
        'except' => [ 'destroy', 'edit', 'create', 'update' ]
    ]);

});

Route::get('/route', 'RouteController@index')->name('route');
