-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.8-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para prueba_ariadna
DROP DATABASE IF EXISTS `prueba_ariadna`;
CREATE DATABASE IF NOT EXISTS `prueba_ariadna` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `prueba_ariadna`;

-- Volcando estructura para tabla prueba_ariadna.document
DROP TABLE IF EXISTS `document`;
CREATE TABLE IF NOT EXISTS `document` (
  `iddocument` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `iduser_create` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`iddocument`),
  UNIQUE KEY `document_numer_unique` (`number`),
  KEY `document_iduser_create_foreign` (`iduser_create`),
  CONSTRAINT `document_iduser_create_foreign` FOREIGN KEY (`iduser_create`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.document: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.menu
DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `idmenu` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `father` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idmenu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.menu: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`idmenu`, `route`, `father`, `name`, `class`, `icon`, `created_at`, `updated_at`) VALUES
	(1, 'home', 0, 'Inicio', 'home-menu', '<i class="flaticon2-layers-1"></i>', '2021-02-01 15:04:06', '2021-02-01 15:37:47'),
	(2, 'roles', 7, 'Roles', 'role-menu', '<i class="flaticon2-group"></i>', '2020-06-29 19:58:29', '2021-02-01 15:08:11'),
	(3, 'permissions', 7, 'Permisos', 'permission-menu', '<i class="flaticon-security"></i>', '2020-06-29 19:59:02', '2021-02-01 15:08:08'),
	(4, 'menus', 7, 'Menús', 'menu-menu', '<i class="flaticon2-layers-2"></i>', '2020-06-29 19:59:33', '2021-02-01 15:08:04'),
	(5, 'users', 7, 'Usuarios', 'user-menu', '<i class="flaticon-avatar"></i>', '2020-06-29 19:59:53', '2021-02-01 15:08:00'),
	(7, '#', 0, 'Configuración', 'configuracion-menu', '<i class="flaticon2-gear"></i>', '2020-06-29 19:42:56', '2020-07-09 15:36:53'),
	(8, 'themes', 0, 'Temas', 'theme-menu', '<i class="flaticon2-tag"></i>', '2021-02-01 15:38:05', '2021-02-01 15:41:04'),
	(9, 'documents', 0, 'Radicado', 'document-menu', '<i class="flaticon2-list-3"></i>', '2021-02-01 15:40:13', '2021-02-01 16:07:16');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.migrations: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(67, '2014_10_12_100000_create_password_resets_table', 1),
	(68, '2019_08_19_000000_create_failed_jobs_table', 1),
	(69, '2020_06_28_174206_create_roles_table', 1),
	(70, '2020_06_29_053636_create_users_table', 1),
	(71, '2020_06_29_151806_create_permission_tree', 1),
	(72, '2020_06_29_154853_create_permission_role', 1),
	(73, '2020_06_29_155101_create_menu', 1),
	(74, '2020_06_29_155329_create_permission_menu', 1),
	(75, '2021_02_01_151256_create_theme_table', 2),
	(78, '2021_02_01_154915_create_document_table', 3),
	(81, '2021_02_01_164007_create_theme_document_table', 4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.permission_menu
DROP TABLE IF EXISTS `permission_menu`;
CREATE TABLE IF NOT EXISTS `permission_menu` (
  `idpermission_menu` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idmenu` bigint(20) unsigned NOT NULL,
  `idrole` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpermission_menu`),
  KEY `permission_menu_idmenu_foreign` (`idmenu`),
  KEY `permission_menu_idrole_foreign` (`idrole`),
  CONSTRAINT `permission_menu_idmenu_foreign` FOREIGN KEY (`idmenu`) REFERENCES `menu` (`idmenu`),
  CONSTRAINT `permission_menu_idrole_foreign` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.permission_menu: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `permission_menu` DISABLE KEYS */;
INSERT INTO `permission_menu` (`idpermission_menu`, `idmenu`, `idrole`, `created_at`, `updated_at`) VALUES
	(28, 7, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16'),
	(29, 2, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16'),
	(30, 3, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16'),
	(31, 4, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16'),
	(32, 5, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16'),
	(33, 1, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16'),
	(34, 9, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16'),
	(35, 8, 1, '2021-02-01 15:41:16', '2021-02-01 15:41:16');
/*!40000 ALTER TABLE `permission_menu` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.permission_role
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `idpermission_role` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idpermission_tree` bigint(20) unsigned NOT NULL,
  `idrole` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpermission_role`),
  KEY `permission_role_idpermission_tree_foreign` (`idpermission_tree`),
  KEY `permission_role_idrole_foreign` (`idrole`),
  CONSTRAINT `permission_role_idpermission_tree_foreign` FOREIGN KEY (`idpermission_tree`) REFERENCES `permission_tree` (`idpermission_tree`),
  CONSTRAINT `permission_role_idrole_foreign` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.permission_role: ~41 rows (aproximadamente)
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` (`idpermission_role`, `idpermission_tree`, `idrole`, `created_at`, `updated_at`) VALUES
	(824, 11, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(825, 12, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(826, 13, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(827, 14, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(828, 15, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(829, 16, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(830, 17, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(831, 18, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(832, 19, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(833, 20, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(834, 21, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(835, 22, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(836, 23, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(837, 24, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(838, 25, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(839, 26, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(840, 27, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(841, 28, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(842, 29, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(843, 30, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(844, 40, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(845, 39, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(846, 35, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(847, 37, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(848, 32, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(849, 41, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(850, 36, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(851, 31, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(852, 42, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(853, 43, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(854, 44, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(855, 45, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(856, 46, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(857, 47, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(858, 48, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(859, 49, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(860, 50, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(861, 51, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(862, 52, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(863, 53, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58'),
	(864, 54, 1, '2021-02-01 16:07:58', '2021-02-01 16:07:58');
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.permission_tree
DROP TABLE IF EXISTS `permission_tree`;
CREATE TABLE IF NOT EXISTS `permission_tree` (
  `idpermission_tree` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `father` int(11) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpermission_tree`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.permission_tree: ~54 rows (aproximadamente)
/*!40000 ALTER TABLE `permission_tree` DISABLE KEYS */;
INSERT INTO `permission_tree` (`idpermission_tree`, `route`, `father`, `status`, `created_at`, `updated_at`) VALUES
	(1, 'Auth', 0, 1, '2020-07-06 04:39:05', '2020-07-09 15:37:18'),
	(2, 'LoginController@showLoginForm', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(3, 'LoginController@login', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(4, 'LoginController@logout', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(5, 'ForgotPasswordController@showLinkRequestForm', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(6, 'ForgotPasswordController@sendResetLinkEmail', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(7, 'ResetPasswordController@showResetForm', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(8, 'ResetPasswordController@reset', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(9, 'ConfirmPasswordController@showConfirmForm', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(10, 'ConfirmPasswordController@confirm', 1, 1, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(11, 'Roles', 0, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(12, 'RolesController@list', 11, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(13, 'RolesController@permission_menu', 11, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(14, 'RolesController@permission', 11, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(15, 'RolesController@index', 11, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(16, 'RolesController@store', 11, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(17, 'RolesController@show', 11, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(18, 'RolesController@update', 11, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(19, 'Menus', 0, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(20, 'MenusController@list', 19, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(21, 'MenusController@index', 19, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(22, 'MenusController@store', 19, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(23, 'MenusController@show', 19, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(24, 'MenusController@update', 19, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(25, 'Permissions', 0, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(26, 'PermissionsController@list', 25, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(27, 'PermissionsController@index', 25, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(28, 'PermissionsController@store', 25, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(29, 'PermissionsController@show', 25, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(30, 'PermissionsController@update', 25, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(31, 'Users', 0, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(32, 'UsersController@list', 31, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(33, 'UsersController@profile_update', 31, 3, '2020-07-06 04:39:05', '2020-07-06 04:44:47'),
	(34, 'UsersController@profile_password', 31, 3, '2020-07-06 04:39:05', '2020-07-06 04:42:35'),
	(35, 'UsersController@update', 31, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(36, 'UsersController@change_status', 31, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(37, 'UsersController@change_password', 31, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(38, 'UsersController@profile', 31, 3, '2020-07-06 04:39:05', '2020-07-06 04:43:35'),
	(39, 'UsersController@index', 31, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(40, 'UsersController@store', 31, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(41, 'UsersController@show', 31, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(42, 'Home', 0, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(43, 'HomeController@index', 42, 2, '2020-07-06 04:39:05', '2020-07-06 04:39:05'),
	(44, 'Themes', 0, 2, '2021-02-01 15:36:37', '2021-02-01 15:36:37'),
	(45, 'ThemesController@list', 44, 2, '2021-02-01 15:36:37', '2021-02-01 15:36:37'),
	(46, 'ThemesController@index', 44, 2, '2021-02-01 15:36:37', '2021-02-01 15:36:37'),
	(47, 'ThemesController@store', 44, 2, '2021-02-01 15:36:37', '2021-02-01 15:36:37'),
	(48, 'ThemesController@show', 44, 2, '2021-02-01 15:36:37', '2021-02-01 15:36:37'),
	(49, 'ThemesController@update', 44, 2, '2021-02-01 15:36:37', '2021-02-01 15:36:37'),
	(50, 'Documents', 0, 2, '2021-02-01 16:06:47', '2021-02-01 16:06:47'),
	(51, 'DocumentsController@destroy', 50, 2, '2021-02-01 16:06:47', '2021-02-01 16:06:47'),
	(52, 'DocumentsController@index', 50, 2, '2021-02-01 16:07:39', '2021-02-01 16:07:39'),
	(53, 'DocumentsController@store', 50, 2, '2021-02-01 16:07:39', '2021-02-01 16:07:39'),
	(54, 'DocumentsController@show', 50, 2, '2021-02-01 16:07:39', '2021-02-01 16:07:39');
/*!40000 ALTER TABLE `permission_tree` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.role
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `idrole` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.role: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`idrole`, `name`, `main_route`, `created_at`, `updated_at`) VALUES
	(1, 'Administrador', 'home.index', NULL, '2021-02-01 15:05:49');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.theme
DROP TABLE IF EXISTS `theme`;
CREATE TABLE IF NOT EXISTS `theme` (
  `idtheme` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idtheme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.theme: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `theme` DISABLE KEYS */;
INSERT INTO `theme` (`idtheme`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Tema 1', '2021-02-01 15:41:27', '2021-02-01 15:42:32'),
	(2, 'tema 2', '2021-02-01 15:42:41', '2021-02-01 15:42:41'),
	(3, 'tema 3', '2021-02-01 15:42:51', '2021-02-01 15:42:51');
/*!40000 ALTER TABLE `theme` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.theme_document
DROP TABLE IF EXISTS `theme_document`;
CREATE TABLE IF NOT EXISTS `theme_document` (
  `idtheme_document` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `iddocument` bigint(20) unsigned NOT NULL,
  `idtheme` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idtheme_document`),
  KEY `theme_document_iddocument_foreign` (`iddocument`),
  KEY `theme_document_idtheme_foreign` (`idtheme`),
  CONSTRAINT `theme_document_iddocument_foreign` FOREIGN KEY (`iddocument`) REFERENCES `document` (`iddocument`) ON DELETE CASCADE,
  CONSTRAINT `theme_document_idtheme_foreign` FOREIGN KEY (`idtheme`) REFERENCES `theme` (`idtheme`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.theme_document: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `theme_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `theme_document` ENABLE KEYS */;

-- Volcando estructura para tabla prueba_ariadna.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `iduser` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `idrole` bigint(20) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE KEY `user_email_unique` (`email`),
  UNIQUE KEY `user_username_unique` (`username`),
  KEY `user_idrole_foreign` (`idrole`),
  CONSTRAINT `user_idrole_foreign` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla prueba_ariadna.user: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`iduser`, `name`, `email`, `username`, `email_verified_at`, `password`, `profile`, `status`, `idrole`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@gmail.com', 'admin', NULL, '$2y$10$D.g8wqnI3ukc91BBtnfuDeF9B73Bk9PYcI1.mVeJrfycbvtZvs/gG', NULL, 1, 1, 'NIlak8v0UyymnP8gG1oRxcXvcfP5eE5m8EOGI3HrB1eFnz02IoxwqcpTpmpz', NULL, '2021-02-01 22:55:36');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
