$(document).ready(function() {

    $(".configuracion-menu").addClass("menu-item-open");
    $(".user-menu").addClass("menu-item-active");

    //Creacion de la tabla users activos,
    var dataTable = $('#users').DataTable({
        language: {
            url: base_url + 'js/plugins/datatables/spanish.json'
        },
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: base_url + "users/list",
            type: "POST",
            data: { "_token": csrf_token },

        },
        "columnDefs": [{
            "targets": [6],
            "orderable": false,
        }, ],
        "rowCallback": function(row, data) {
            $(row).addClass('xs-block');
            $('td:nth-child(1)', row).attr("data-title", "ID");
            $('td:nth-child(2)', row).attr("data-title", "Nombre");
            $('td:nth-child(3)', row).attr("data-title", "Email");
            $('td:nth-child(4)', row).attr("data-title", "Nombre Usuario");
            $('td:nth-child(5)', row).attr("data-title", "Rol");
            $('td:nth-child(6)', row).attr("data-title", "Estado");
            $('td:nth-child(7)', row).attr("data-title", "Acción");
        }
    });

    //Validate para validar los datos del formulario ingresar  
    $("#form_create").validate({

        submitHandler: function(form) {

            var formData = new FormData(document.getElementById("form_create"));

            $.ajaxSetup({
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='csrf-token']").attr('content'));
                }
            });

            $.ajax({
                url: base_url + 'users',
                type: "POST",
                dataType: "json",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
            }).done(function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $("input[type=text], input[type=password], select").val("");
                $(".cancel-profile").click();
                $('#userModal').modal('hide');
                return mensaje_swal_success('¡Ingresado!', response.msg, 'success');
            });
        }
    });

    $(document).on('click', '.create', function(e) {
        $('#userModal').modal('show');
    });


    $(document).on('click', '.change_status', function(e) {
        e.preventDefault();
        $('#userStatusModal').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('users/' + id, data, function(response) {

            $("#id_user_status").val(response.iduser);
            $("#name_update").val(response.name);

            if (response.status == $USER_ACTIVE) {
                $("#userStatusModalLabel").text("¿Esta seguro de inactivar el usuario '" + response.name + "'?");
                $(".userButtonModalLabel").text("Inactivar usuario");
            } else {
                $("#userStatusModalLabel").text("¿Esta seguro de activar el usuario '" + response.name + "'?");
                $(".userButtonModalLabel").text("Activar usuario");
            }

        }, 'GET');
    });

    //Mostrar la ventana modal y cargar los datos en la ventana modal del validate
    $(document).on('click', '.update', function(e) {
        e.preventDefault();
        $('#userModalUpdate').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('users/' + id, data, function(response) {

            $("#name_update").val(response.name);
            $("#username_update").val(response.username);
            $("#email_update").val(response.email);
            $("#idrole_update").val(response.idrole);
            $("#id_user_update").val(response.iduser);

            $("#name_update").val(response.name);

            if (response.profile == '' || response.profile == null) {
                $("#user_edit_update_avatar").css("background-image", "url(" + assets_url + "img/profile_photo.png" + ")");
            } else {
                $("#user_edit_update_avatar").css("background-image", "url(" + assets_url + "images/" + response.profile + ")");
            }

        }, 'GET');
    });

    //Validate para modificar el usuario con sus datos ya cargados.
    $("#form_update").validate({
        submitHandler: function(form) {

            let id = $("#id_user_update").val();
            var data = new FormData(document.getElementById("form_update"));

            $.ajaxSetup({
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='csrf-token']").attr('content'));
                }
            });

            $.ajax({
                url: base_url + 'users/' + id,
                type: "POST",
                dataType: "json",
                data: data,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $(".cancel-profile-update").click();
                $('#userModalUpdate').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');
            });
        }
    });

    $("#form_status").validate({
        submitHandler: function(form) {

            var data = {};

            let id = document.getElementById("id_user_status").value.trim();

            ajax('users/change_status/' + id, data, function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $('#userStatusModal').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');
            }, 'GET');

        }
    });

    $(document).on('click', '.change_password', function(e) {
        e.preventDefault();
        $('#userPasswordModal').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('users/' + id, data, function(response) {
            $("#id_user_password").val(response.iduser);
        }, 'GET');
    });

    $("#form_password").validate({
        submitHandler: function(form) {

            var data = {};

            let id = document.getElementById("id_user_password").value.trim();
            data.password = $("#change_password").val().trim();
            ajax('users/change_password/' + id, data, function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                $("input[type=text], input[type=password], select").val("");
                $('#userPasswordModal').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');
            }, 'POST');

        }
    });
});