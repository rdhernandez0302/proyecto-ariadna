$(document).ready(function() {

    $(".parameters-menu").addClass("menu-item-open");
    $(".theme-menu").addClass("menu-item-active");

    //Creacion de la tabla themes activos,
    var dataTable = $('#themes').DataTable({
        language: {
            url: base_url + 'js/plugins/datatables/spanish.json'
        },
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: base_url + "themes/list",
            type: "POST",
            data: { "_token": csrf_token },

        },
        "columnDefs": [{
            "targets": [2],
            "orderable": false,
        }, ],
        "rowCallback": function(row, data) {
            $(row).addClass('xs-block');
            $('td:nth-child(1)', row).attr("data-title", "ID");
            $('td:nth-child(2)', row).attr("data-title", "Nombre");
            $('td:nth-child(3)', row).attr("data-title", "Acción");
        }
    });


    //Validate para validar los datos del formulario ingresar
    $("#form_create").validate({

        submitHandler: function(form) {
            var data = {};

            data.name = document.getElementById("name").value.trim();

            ajax('themes', data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $("input[type=text]").val("");
                $('#themeModal').modal('hide');
                return mensaje_swal_success('¡Ingresado!', response.msg, 'success');
            }, 'POST');
        }
    });

    $(document).on('click', '.create', function(e) {
        $('#themeModal').modal('show');
    });

    //Mostrar la ventana modal y cargar los datos en la ventana modal del validate
    $(document).on('click', '.update', function(e) {
        e.preventDefault();
        $('#themeModalUpdate').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('themes/' + id, data, function(response) {

            $("#name_update").val(response.name);
            $("#id_theme_update").val(response.idtheme);

        }, 'GET');
    });

    //Validate para modificar el tema con sus datos ya cargados.
    $("#form_update").validate({
        submitHandler: function(form) {
            var data = {};

            let id = $("#id_theme_update").val();
            data.name = document.getElementById("name_update").value.trim();

            ajax('themes/' + id, data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $('#themeModalUpdate').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');

            }, 'PUT');
        }
    });

});
