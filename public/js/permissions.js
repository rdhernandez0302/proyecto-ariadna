$(document).ready(function() {

    $(".configuracion-menu").addClass("menu-item-open");
    $(".permission-menu").addClass("menu-item-active");

    //Creacion de la tabla permissions activos,
    var dataTable = $('#permissions').DataTable({
        language: {
            url: base_url + 'js/plugins/datatables/spanish.json'
        },
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: base_url + "permissions/list",
            type: "POST",
            data: { "_token": csrf_token },

        },
        "columnDefs": [{
            "targets": [4],
            "orderable": false,
        }, ],
        "rowCallback": function(row, data) {
            $(row).addClass('xs-block');
            $('td:nth-child(1)', row).attr("data-title", "ID");
            $('td:nth-child(2)', row).attr("data-title", "Ruta");
            $('td:nth-child(3)', row).attr("data-title", "Estado");
            $('td:nth-child(4)', row).attr("data-title", "Padre");
            $('td:nth-child(5)', row).attr("data-title", "Acción");
        }
    });


    //Validate para validar los datos del formulario ingresar  
    $("#form_create").validate({

        submitHandler: function(form) {
            var data = {};

            data.route = document.getElementById("route").value.trim();
            data.status = document.getElementById("status").value.trim();
            data.father = $("#father").val().trim();

            ajax('permissions', data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $("input[type=text]").val("");
                $('#permissionModal').modal('hide');
                return mensaje_swal_success('¡Ingresado!', response.msg, 'success');
            }, 'POST');
        }
    });

    $(document).on('click', '.create', function(e) {
        $('#permissionModal').modal('show');
    });

    //Mostrar la ventana modal y cargar los datos en la ventana modal del validate
    $(document).on('click', '.update', function(e) {
        e.preventDefault();
        $('#permissionModalUpdate').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('permissions/' + id, data, function(response) {

            $("#route_update").val(response.route);
            $("#status_update").val(response.status);
            $("#father_update").val(response.father);
            $("#id_permission_update").val(response.idpermission_tree);

        }, 'GET');
    });

    //Validate para modificar el usuario con sus datos ya cargados.
    $("#form_update").validate({
        submitHandler: function(form) {
            var data = {};

            let id = $("#id_permission_update").val();
            data.route = document.getElementById("route_update").value.trim();
            data.status = document.getElementById("status_update").value.trim();
            data.father = $("#father_update").val().trim();


            ajax('permissions/' + id, data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $('#permissionModalUpdate').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');

            }, 'PUT');
        }
    });
});