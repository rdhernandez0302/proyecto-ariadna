$(document).ready(function() {

    $(".parameters-menu").addClass("menu-item-open");
    $(".document-menu").addClass("menu-item-active");

    $('#themes-add').select2({
        placeholder: "Seleccion el tema",
        allowClear: true
    });

    $('#date-add').datetimepicker({
        format: 'Y-m-d H:i',
        formatDate: 'Y-m-d H:i',
        scroll: false,
        maxDate: new Date('2013-12-31'),
        minDate: new Date('2007-01-01'),
        defaultDate: new Date('2013-12-31')
    });

    //Validate para validar los datos del formulario ingresar
    $("#form_create").validate({


        submitHandler: function(form) {
            var data = {};
            data.number = $("#number-add").val().trim();
            data.title = $("#title-add").val().trim();
            data.date =  $("#date-add").val().trim();

            var themes = [];
            $('#themes-add option:checked').each(function() {
                themes.push($(this).val());
            });

            data.themes = themes;

            ajax('documents', data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                $('#themes-add').val("").trigger('change.select2');
                $("input[type=text]").val("");
                $('#documentModal').modal('hide');
                return mensaje_swal_success('¡Ingresado!', response.msg, 'success');
            }, 'POST');
        }
    });

    $(document).on('click', '.create', function(e) {
        $('#documentModal').modal('show');
    });

    //Mostrar la ventana modal y cargar los datos en la ventana modal del validate
    $(document).on('click', '#btn-search-document', function(e) {
        loadDocument();
    });

    function loadDocument() {
        let number_document = $("#input-search-document").val();

        if(number_document == null || number_document == ""){
            Swal.fire({
                title: '¡Alerta!',
                icon: 'error',
                text: "Por favor ingrese el número de radicado"
            }).then( (e) => {
                $("#input-search-document").focus();
            });

            return;
        }

        var data = {};
        ajax('documents/' + number_document, data, function(response) {

            $(".card-document").addClass("d-none");
            $(".container-themes span").remove();
            if (response.res == $EXIT_ERROR) {

                if (response.msg.length > 0) {
                    Swal.fire({
                        title: '¡Alerta!',
                        icon: 'error',
                        text: response.msg[0][0]
                    });
                    return;
                }
            }

            $(".card-document").removeClass("d-none");
            $(".title").text(response.title);
            $(".date").text(response.date);
            $(".created_at").text("by " + response.nameUser);

            $(".number").text("No. " + response.number);
            $(".btn-delete-document").attr("id-document", response.iddocument);

            if(response.themes.length == 0){
                $(".container-themes").append(`<span class="opacity-90">No hay temas registrados</span>`);
            }

            for (let i = 0; i < response.themes.length; i++) {
                $(".container-themes").append(`<span class="opacity-90">${response.themes[i]["name"]}</span>`);
            }

        }, 'GET');
    }


    $(document).on('click', '.btn-delete-document', function(e) {

        Swal.fire({

            title: '¡Alerta!',
            icon: 'info',
            text: '¿Esta seguro de eliminar el radicado?',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: '¡Si, Eliminarlo!',
            cancelButtonText: "No, cancelarlo"
        }).then( (value) => {

            if (value.isConfirmed){

                let id = $(this).attr("id-document");
                var data = {};

                ajax('documents/' + id, data, function(response) {

                    if (response.res == $EXIT_ERROR) {

                        if (response.msg.length > 0) {
                            Swal.fire({
                                title: '¡Alerta!',
                                icon: 'error',
                                text: response.msg[0][0]
                            });
                            return;
                        }
                    }

                    $(".card-document").addClass("d-none");
                    $(".container-themes span").remove();
                    $("#input-search-document").val("")

                }, 'DELETE');
            }

        });
    });

    $("#input-search-document, #number-add").keypress(function (e) {

        if($(this).attr("id") == "input-search-document" && e.which == 13) {
            loadDocument();
        }

        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });

});
