var UserEdit = { init: function() { new KTImageInput("user_edit") } };


$(document).ready(function() {
    UserEdit.init();

    //Validate para modificar el usuario con sus datos ya cargados.
    $("#form_profile").validate({
        submitHandler: function(form) {

            var data = new FormData(document.getElementById("form_profile"));

            $.ajaxSetup({
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='csrf-token']").attr('content'));
                }
            });

            $.ajax({
                url: base_url + 'users/profile_update',
                type: "POST",
                dataType: "json",
                data: data,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                return mensaje_swal_success('¡Modificado!', response.msg, 'success');
            });
        }
    });

    $("#form_profile_password").validate({
        submitHandler: function(form) {

            var data = new FormData(document.getElementById("form_profile_password"));

            $.ajaxSetup({
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-CSRF-TOKEN', $("meta[name='csrf-token']").attr('content'));
                }
            });

            $.ajax({
                url: base_url + 'users/profile_password',
                type: "POST",
                dataType: "json",
                data: data,
                cache: false,
                contentType: false,
                processData: false
            }).done(function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                $("input[type=password").val("");
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');
            });
        }
    });



});