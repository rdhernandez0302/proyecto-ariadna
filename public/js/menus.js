$(document).ready(function() {

    $(".configuracion-menu").addClass("menu-item-open");
    $(".menu-menu").addClass("menu-item-active");

    //Creacion de la tabla menus activos,
    var dataTable = $('#menus').DataTable({
        language: {
            url: base_url + 'js/plugins/datatables/spanish.json'
        },
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: base_url + "menus/list",
            type: "POST",
            data: { "_token": csrf_token },

        },
        "columnDefs": [{
            "targets": [6],
            "orderable": false,
        }, ],
        "rowCallback": function(row, data) {
            $(row).addClass('xs-block');
            $('td:nth-child(1)', row).attr("data-title", "ID");
            $('td:nth-child(2)', row).attr("data-title", "Ruta");
            $('td:nth-child(3)', row).attr("data-title", "Nombre");
            $('td:nth-child(4)', row).attr("data-title", "Clase");
            $('td:nth-child(5)', row).attr("data-title", "Icono");
            $('td:nth-child(6)', row).attr("data-title", "Padre");
            $('td:nth-child(7)', row).attr("data-title", "Acción");
        }
    });


    //Validate para validar los datos del formulario ingresar  
    $("#form_create").validate({

        submitHandler: function(form) {
            var data = {};

            data.route = document.getElementById("route").value.trim();
            data.name = document.getElementById("name").value.trim();
            data.class = document.getElementById("class").value.trim();
            data.icon = document.getElementById("icon").value.trim();
            data.father = $("#father").val().trim();

            ajax('menus', data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $("input[type=text]").val("");
                $('#menuModal').modal('hide');
                return mensaje_swal_success('¡Ingresado!', response.msg, 'success');
            }, 'POST');
        }
    });

    $(document).on('click', '.create', function(e) {
        $('#menuModal').modal('show');
    });

    //Mostrar la ventana modal y cargar los datos en la ventana modal del validate
    $(document).on('click', '.update', function(e) {
        e.preventDefault();
        $('#menuModalUpdate').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('menus/' + id, data, function(response) {

            $("#route_update").val(response.route);
            $("#name_update").val(response.name);
            $("#class_update").val(response.class);
            $("#icon_update").val(response.icon);
            $("#father_update").val(response.father);
            $("#id_menu_update").val(response.idmenu);

        }, 'GET');
    });

    //Validate para modificar el usuario con sus datos ya cargados.
    $("#form_update").validate({
        submitHandler: function(form) {
            var data = {};

            let id = $("#id_menu_update").val();
            data.route = document.getElementById("route_update").value.trim();
            data.name = document.getElementById("name_update").value.trim();
            data.class = document.getElementById("class_update").value.trim();
            data.icon = document.getElementById("icon_update").value.trim();
            data.father = $("#father_update").val().trim();

            ajax('menus/' + id, data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $('#menuModalUpdate').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');

            }, 'PUT');
        }
    });


});