$(document).ready(function() {

    $(".configuracion-menu").addClass("menu-item-open");
    $(".role-menu").addClass("menu-item-active");

    $('#jstree_menu, #jstree').jstree({
        "plugins": ["wholerow", "checkbox", "types"],
        "types": {
            "default": {
                "icon": "fa fa-folder text-warning"
            },
            "file": {
                "icon": "fa fa-file  text-warning"
            }
        },
    });

    //Creacion de la tabla roles activos,
    var dataTable = $('#roles').DataTable({
        language: {
            url: base_url + 'js/plugins/datatables/spanish.json'
        },
        "pagingType": "full_numbers",
        "processing": true,
        "serverSide": true,
        "order": [
            [0, "asc"]
        ],
        "ajax": {
            url: base_url + "roles/list",
            type: "POST",
            data: { "_token": csrf_token },

        },
        "columnDefs": [{
            "targets": [2],
            "orderable": false,
        }, ],
        "rowCallback": function(row, data) {
            $(row).addClass('xs-block');
            $('td:nth-child(1)', row).attr("data-title", "ID");
            $('td:nth-child(2)', row).attr("data-title", "Rol");
            $('td:nth-child(3)', row).attr("data-title", "Acción");
        }
    });

    //Validate para validar los datos del formulario ingresar  
    $("#form_create").validate({

        submitHandler: function(form) {
            var data = {};

            data.name = document.getElementById("name").value.trim();
            data.main_route = document.getElementById("main_route").value.trim();

            ajax('roles', data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $("input[type=text]").val("");
                $('#roleModal').modal('hide');
                return mensaje_swal_success('¡Ingresado!', response.msg, 'success');
            }, 'POST');
        }
    });

    $(document).on('click', '.create', function(e) {
        $('#roleModal').modal('show');
    });

    //Mostrar la ventana modal y cargar los datos en la ventana modal del validate
    $(document).on('click', '.update', function(e) {
        e.preventDefault();
        $('#roleModalUpdate').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('roles/' + id, data, function(response) {

            $("#name_update").val(response.name);
            $("#main_route_update").val(response.main_route);
            $("#id_role_update").val(response.idrole);

        }, 'GET');
    });

    //Validate para modificar el usuario con sus datos ya cargados.
    $("#form_update").validate({
        submitHandler: function(form) {
            var data = {};

            let id = $("#id_role_update").val();
            data.name = document.getElementById("name_update").value.trim();
            data.main_route = document.getElementById("main_route_update").value.trim();

            ajax('roles/' + id, data, function(response) {

                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                dataTable.ajax.reload();
                $('#roleModalUpdate').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');

            }, 'PUT');
        }
    });

    //Modal de permisos
    $(document).on('click', '.assing-permissions-menu', function(e) {
        e.preventDefault();

        $('#permissionMenuModal').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('roles/' + id, data, function(response) {

            $('#jstree_menu').jstree(true).deselect_all(true);

            if (response.permissionMenu.length > 0) {

                for (var i = 0; i < response.permissionMenu.length; i++) {
                    $('#jstree_menu').jstree(true).select_node("APRM_" + response.permissionMenu[i]["idmenu"] + "");
                }
            }

            $("#id_role_permission_menu").val(response.idrole);

        }, 'GET');
    });

    $("#form_permission_menu").validate({
        submitHandler: function(form) {
            var data = {};

            let id = document.getElementById("id_role_permission_menu").value.trim();
            data.permissions = $('#jstree_menu').jstree(true).get_selected();

            ajax('roles/update_permission_menu/' + id, data, function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                $('#permissionMenuModal').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');
            }, 'PUT');
        }
    });

    //Modal de permisos
    $(document).on('click', '.assing-permissions', function(e) {
        e.preventDefault();

        $('#permissionModal').modal('show');

        let id = $(this).attr("id");
        var data = {};
        ajax('roles/' + id, data, function(response) {

            $('#jstree').jstree(true).deselect_all(true);

            if (response.permission.length > 0) {

                for (var i = 0; i < response.permission.length; i++) {
                    $('#jstree').jstree(true).select_node("APRM_" + response.permission[i]["idpermission_tree"] + "");
                }
            }

            $("#id_role_permission").val(response.idrole);

        }, 'GET');

    });

    $("#form_permission").validate({
        submitHandler: function(form) {

            var data = {};

            let id = document.getElementById("id_role_permission").value.trim();
            data.permissions = $('#jstree').jstree(true).get_selected();

            ajax('roles/update_permission/' + id, data, function(response) {
                if (response.res == $EXIT_ERROR) {

                    if (response.msg.length > 0) {
                        Swal.fire({
                            title: '¡Alerta!',
                            icon: 'error',
                            text: response.msg[0][0]
                        });
                        return;
                    }
                }

                $('#permissionModal').modal('hide');
                return mensaje_swal_success('¡Modificado!', response.msg, 'success');
            }, 'PUT');

        }
    });

});