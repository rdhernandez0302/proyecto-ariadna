<?php
    function botonera($functions){

        $listButtons = "";
    	
    	for ($i=0; $i < count( $functions ); $i++) {    

            $dataPermissionParent = array(
                "route" => $functions[$i]["method"],
                "idrole" => Auth::user()->idrole,
            );

            $listPermissions = \App\Models\PermissionTree::HasPermissionByParent($dataPermissionParent);

    		if( count( $listPermissions ) > 0 && ! empty($functions[$i]["method"]) ) {

    			$text			= ( isset( $functions[$i]["text"] ) ) 		? $functions[$i]["text"] 		: "";
    			$attributes 	= ( isset( $functions[$i]["attributes"] ) ) ? $functions[$i]["attributes"] 	: "";
    			$ruta 		    = ( isset( $functions[$i]["ruta"] ) ) 		? $functions[$i]["ruta"] 		: "";
    			$name 		    = ( isset( $functions[$i]["name"] ) ) 		? $functions[$i]["name"] 		: "";
    			$id 			= ( isset( $functions[$i]["id"] ) ) 		? $functions[$i]["id"] 			: "";
    			$classes 		= ( isset( $functions[$i]["classes"] ) ) 	? $functions[$i]["classes"] 	: "";
                $title 	     	= ( isset( $functions[$i]["title"] ) ) 		? $functions[$i]["title"] 		: "";
                $icon 	     	= ( isset( $functions[$i]["icon"] ) ) 		? $functions[$i]["icon"] 		: "";

    			if( empty( $functions[$i]["link"] ) ) {
                    $listButtons .= '<button name="' . $name . '" id="' . $id . '" class="btn btn-shadow font-weight-bold mr-2 ' . $classes  . '" title="' . $title . '"    data-tooltip="tooltip" ' . $attributes . ' >
                            <i class="' . $icon . '"></i>
                            ' . $text  . '
                        </button>';
            	}else{
                    $listButtons .= '<a href="' . $ruta . '" name="' . $name . '" id="' . $id . '" class="btn btn-shadow font-weight-bold mr-2 ' . $classes  . '" title="' . $title . '"    data-tooltip="tooltip" ' . $attributes . ' >
                        <i class="' . $icon . '"></i>
                        ' . $text  . '
                    </a> ';   
            	}
            }
    	}
    	return $listButtons;
        
    }   


?>