<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\PermissionRole;
use App\Models\PermissionTree;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //return $next($request);
        if ( ! Auth::check()) {
            if ($request->ajax()) {
                $msg[0][0] = "Lo sentimos su sesión ya termino.";
                $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                return response()->json($data, 200);
            } else {
                return redirect()->guest('/login');
            }
        }

        $route = explode("\\", $request->route()->getActionName()); 

        
        if(count($route) > 2) {

            $route = array_values(array_slice($route, -3));

            if($route[0] == "Controllers"){
                
                $permissionTree = PermissionTree::where('route', $route[2])->first();
                
                if(empty($permissionTree)){
                    if ($request->ajax()) {
                        $msg[0][0] = "Lo sentimos la ruta no existe.";
                        $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                        return response()->json($data, 200);
                    } else {
                        return response()->view('errors.404', [], 404);
                    }
                }

                if($permissionTree->status != config("global.PERMISSION_TREE_PROTECTED")){

                    $data = [
                        "idpermission_tree" => $permissionTree->idpermission_tree,
                        "idrole"            => Auth::user()->idrole,
                    ];

                    $permissionTree = PermissionRole::where($data)->first();     
                    
                    if(empty($permissionTree)){
                        if ($request->ajax()) {
                            $msg[0][0] = "Lo sentimos no tiene permisos para realizar esta acción.";
                            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                            return response()->json($data, 200);
                        } else {
                            return response()->view('errors.404', [], 404);
                        }
                    }
                }
            }   
        }

        return $next($request);
    }
}
