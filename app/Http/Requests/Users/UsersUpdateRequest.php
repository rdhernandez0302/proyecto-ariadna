<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class UsersUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required",
            "email" => ["required", "string", "email", "max:255", "unique:user,email,".$this->id.",iduser"],
            "username" => ["required", "string", "max:255", "unique:user,username,".$this->id.",iduser"],
            "idrole" => "required",
            "profile" => ["image", "mimes:jpeg,png,jpg,gif,svg", "max:2048"],
        ];
    }

    public function attributes()
    {
        return [
            "email" => "correo electronico",
            "username" => "nombre de usuario",
        ];
    }
}
