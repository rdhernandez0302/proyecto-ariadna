<?php

namespace App\Http\Requests\Permissions;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PermissionsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "route" => "required",
            "father" => "required",
            "status" => [
                "required",
                Rule::in([config("global.PERMISSION_TREE_PUBLIC"), config("global.PERMISSION_TREE_PROTECTED"), config("global.PERMISSION_TREE_PRIVATE")]),
            ],
        ];
    }
}
