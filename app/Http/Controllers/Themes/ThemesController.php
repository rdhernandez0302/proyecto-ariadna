<?php

namespace App\Http\Controllers\Themes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Themes;
use App\Http\Requests\Themes\ThemesCreateRequest;

class ThemesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $function = [
            [
                "method" 		=> "ThemesController@store",
                "name"			=> "create",
                "classes" 		=> "btn-primary create",
                "title" 		=> "Crear",
                "attributes" 	=> "",
                "icon"          => "fa fa-plus",
                "text"          => "Crear",
                "link" 			=> FALSE
            ]
        ];

        $buttons = botonera($function);

        $data = [
            "buttons" => $buttons
        ];

        return view("themes.index")->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {

        $input = $request->all();

        $theme = Themes::ListByFilters($input);
        $countChannel = Themes::CountListByFilters($input);

        $datas =  array();
        $filtered_rows = count($theme);
        foreach($theme as $row)
        {

            $sub_array = array();
            $sub_array[] = $row->idtheme;
            $sub_array[] = $row->name;

            $function = [
                [
                    "method" 		=> "ThemesController@update",
                    "name"			=> "update",
                    "id"			=> $row->idtheme,
                    "classes" 		=> "btn-icon btn-success update",
                    "title" 		=> "Modificar",
                    "attributes" 	=> "",
                    "icon"          => "flaticon-edit",
                    "link" 			=> FALSE
                ]
            ];

            $buttons = botonera($function);

            $sub_array[] = $buttons;

            $datas[] = $sub_array;
        }

        $output = array(
            "draw"              =>  intval( $input["draw"] ),
            "recordsTotal"      =>  $filtered_rows,
            "recordsFiltered"   =>  $countChannel,
            "data"              =>  $datas
        );

        echo json_encode($output);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThemesCreateRequest $request)
    {
        $input = $request->all();

        $validator = $request->validated();

        $theme = Themes::create($validator);

        $theme->save();

        if(empty($theme)){
            $msg[0][0] = "El tema no se ingreso correctamente." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El tema fue ingresado correctamente" );
        return response()->json($data, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $theme = Themes::find($id);
        return response()->json($theme, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ThemesCreateRequest $request, $id)
    {
        $theme = Themes::find($id);

        if(empty($theme)){
            $msg[0][0] = "El tema ingresado no se encuentra registrada." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $input = $request->all();

        $validator = $request->validated();
        $theme = Themes::where("idtheme", $id)->update($validator);

        if(empty($theme)){
            $msg[0][0] = "El tema no se modifico correctamente." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El tema fue modificado correctamente" );
        return response()->json($data, 200);
    }
}
