<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Users\UsersCreateRequest;
use App\Http\Requests\Users\UsersUpdateRequest;
use App\Http\Requests\Users\UsersPasswordRequest;
use App\Http\Requests\Users\UsersProfileRequest;
use App\Http\Requests\Users\UsersProfilePasswordRequest;
use App\Http\Requests\Users\UsersChangePasswordRequest;
use App\Models\User;
use App\Models\Roles;
use Illuminate\Support\Facades\Hash;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $function = [
            [
                "method" 		=> "UsersController@store",
                "name"			=> "create",
                "classes" 		=> "btn-primary create",
                "title" 		=> "Crear",
                "attributes" 	=> "",
                "icon"          => "fa fa-plus",
                "text"          => "Crear",
                "link" 			=> FALSE
            ]
        ];

        $buttons = botonera($function);

        $roles= Roles::all();
        $data = [
            "roles"  => $roles,
            "buttons" => $buttons
        ];

        return view("users.index")->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {

        $input = $request->all();

        $users = User::ListByFilters($input);
        $countUsers = User::CountListByFilters($input);

        $datas =  array();
        $filtered_rows = count($users);
        foreach($users as $row)
        {
            $status = "Inactivo";
            $classStatus = "warning";
            $icon = "far fa-eye";
            $textButton = "Activar";

            if($row->status == config("global.USER_ACTIVE") ){
                $status = "Activo";
                $classStatus = "success";
                $icon = "far fa-eye-slash";
                $textButton = "Inactivar";
            }

            $sub_array = array();
            $sub_array[] = $row->iduser;
            $sub_array[] = $row->name;
            $sub_array[] = $row->email;
            $sub_array[] = $row->username;
            $sub_array[] = $row->name_rol;
            $sub_array[] = "<span class='label font-weight-bold label-lg label-light-".$classStatus." label-inline'>".$status."</span>";

            $function = [
                [
                    "method" 		=> "UsersController@update",
                    "name"			=> "update",
                    "id"			=> $row->iduser,
                    "classes" 		=> "btn-icon btn-success update",
                    "title" 		=> "Modificar",
                    "attributes" 	=> "",
                    "icon"          => "flaticon-edit",
                    "link" 			=> FALSE
                ],
                [
                    "method" 		=> "UsersController@change_status",
                    "name"			=> "change_status",
                    "id"			=> $row->iduser,
                    "classes" 		=> "btn-icon btn-warning change_status",
                    "title" 		=> $textButton,
                    "attributes" 	=> "",
                    "icon"          => $icon,
                    "link" 			=> FALSE
                ],
                [
                    "method" 		=> "UsersController@change_password",
                    "name"			=> "change_password",
                    "id"			=> $row->iduser,
                    "classes" 		=> "btn-icon btn-primary change_password",
                    "title" 		=> "Cambiar contraseña",
                    "attributes" 	=> "",
                    "icon"          => 'flaticon-lock',
                    "link" 			=> FALSE
                ],
            ];

            $buttons = botonera($function);

            $sub_array[] = $buttons;

            $datas[] = $sub_array;
        }

        $output = array(
            "draw"              =>  intval( $input["draw"] ),
            "recordsTotal"      =>  $filtered_rows,
            "recordsFiltered"   =>  $countUsers,
            "data"              =>  $datas
        );

        echo json_encode($output);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersCreateRequest $request)
    {
        $input = $request->all();

        $validator = $request->validated();

        if( ! empty($validator["profile"])){
            $imageName = time().'.'.$request->profile->extension();
            $request->profile->move(public_path('images'), $imageName);
            $validator["profile"] = $imageName;

        }

        $validator["password"] = Hash::make($validator["password"]);
        $user = User::create($validator);

        $user->save();

        if(empty($user)){
            $msg[0][0] = "El usuario no se ingreso registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El usuario fue ingresado correctamente" );
        return response()->json($data, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return response()->json($user, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersUpdateRequest $request, $id)
    {
        $user = User::find($id);

        if(empty($user)){
            $msg[0][0] = "El usuario ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $validator = $request->validated();

        if( ! empty($validator["profile"])){
            $imageName = time().'.'.$request->profile->extension();
            $request->profile->move(public_path('images'), $imageName);
            $validator["profile"] = $imageName;
        }

        $user = User::where("iduser", $id)->update($validator);

        if(empty($user)){
            $msg[0][0] = "El usuario no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El usuario fue modificado correctamente" );
        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_status($id)
    {

        $user = User::find($id);

        if(empty($user)){
            $msg[0][0] = "El usuario ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $statusName = "inactivado";
        $status = config("global.USER_INACTIVE");

        if($user->status == config("global.USER_INACTIVE")){
            $statusName = "activado";
            $status = config("global.USER_ACTIVE");
        }

        $user = User::where("iduser", $id)->update(["status" => $status]);

        if(empty($user)){
            $msg[0][0] = "El usuario no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El usuario fue " . $statusName . " correctamente" );

        return response()->json($data, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change_password(UsersPasswordRequest $request, $id)
    {
        $user = User::find($id);

        if(empty($user)){
            $msg[0][0] = "El usuario ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $validator = $request->validated();
        $user = User::where("iduser", $id)->update([ "password" => Hash::make($validator["password"]) ]);

        if(empty($user)){
            $msg[0][0] = "El usuario no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El usuario fue actualizado correctamente" );
        return response()->json($data, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view("users.profile");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile_update(UsersProfileRequest $request)
    {
        $validator = $request->validated();

        if( ! empty($validator["profile"])){
            $imageName = time().'.'.$request->profile->extension();
            $request->profile->move(public_path('images'), $imageName);
            $validator["profile"] = $imageName;
        }

        $user = User::where("iduser", Auth::user()->iduser)->update($validator);

        if(empty($user)){
            $msg[0][0] = "El usuario no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El usuario fue modificado correctamente" );
        return response()->json($data, 200);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile_password(UsersChangePasswordRequest $request)
    {
        $validator = $request->validated();
        $credentials = [
            "password" => $validator["current_password"],
            "email" => Auth::user()->email
        ];

        if( ! Auth::validate($credentials)){
            $msg[0][0] = "La contraseña ingresada no coincide con la registrada en el sistema." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = [
            "password" => Hash::make($validator["new_password"])
        ];

        $user = User::where("iduser", Auth::user()->iduser)->update($data);

        if(empty($user)){
            $msg[0][0] = "El usuario no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El usuario fue modificado correctamente" );
        return response()->json($data, 200);
    }
}
