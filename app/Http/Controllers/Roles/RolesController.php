<?php

namespace App\Http\Controllers\Roles;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Roles;
use App\Models\Menus;
use App\Models\PermissionMenu;
use App\Models\PermissionRole;
use App\Models\PermissionTree;
use App\Http\Requests\Roles\RolesCreateRequest;

class RolesController extends Controller
{

    public function treePermissionMenuList($father = 0, $user_tree_array = array()) 
  	{

        $listPermission = Menus::where("father", $father)->get();
        
	    if ( ! empty($listPermission)) 
	    {
            $user_tree_array[] = "<ul>";
            foreach ($listPermission as $row) {

                $user_tree_array[] = "<li id='APRM_" . $row->idmenu . "'>". $row->name;	        
                $user_tree_array = $this->treePermissionMenuList($row->idmenu, $user_tree_array);
                $user_tree_array[] = "</li>";
            }

            $user_tree_array[] = "</ul>";
	    }
	    return $user_tree_array;
    }
    
    public function treePermissionList($father = 0, $user_tree_array = array()) 
  	{

        $listPermission = PermissionTree::where("father", $father)->get();
        
	    if ( ! empty($listPermission)) 
	    {
            $user_tree_array[] = "<ul>";
            foreach ($listPermission as $row) {

                $validatePermissions = FALSE;

                if($father == 0){
                    $data_permisos = array("status" => config("global.PERMISSION_TREE_PRIVATE"), "father" => $row->father);
                    $listPermission = PermissionTree::where($data_permisos)->get();

                    if( ! empty($consultar_padre)){
                        $validador_permisos = TRUE;
                    }
                }

                if($row->status == config("global.PERMISSION_TREE_PRIVATE")){
                    $validatePermissions = TRUE;
                }

                if( ! empty($validatePermissions)) {
                    $user_tree_array[] = "<li id='APRM_" . $row->idpermission_tree . "'>". $row->route;	        
                    $user_tree_array = $this->treePermissionList($row->idpermission_tree, $user_tree_array);
                    $user_tree_array[] = "</li>";
                }
            }

            $user_tree_array[] = "</ul>";
	    }
	    return $user_tree_array;
	}


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

        $function = [
            [
                "method" 		=> "RolesController@store", 
                "name"			=> "create",  
                "classes" 		=> "btn-primary create", 
                "title" 		=> "Crear",
                "attributes" 	=> "",
                "icon"          => "fa fa-plus",
                "text"          => "Crear",
                "link" 			=> FALSE
            ]
        ];

        $buttons = botonera($function);

        $treePermissionMenu = $this->treePermissionMenuList();
        $treePermissions = $this->treePermissionList(); 
        $data = [
            "treePermissions"  => $treePermissions,
            "treePermissionMenu"   => $treePermissionMenu,
            "buttons" => $buttons
        ];

        return view("roles.index")->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {   
        $input = $request->all();
        
        $roles = Roles::ListByFilters($input);
        $countRoles = Roles::CountListByFilters($input);

        $datas =  array();
        $filtered_rows = count($roles);
        foreach($roles as $row)
        {
            $sub_array = array();
            $sub_array[] = $row->idrole;
            $sub_array[] = $row->name;
            
            $function = [
                [
                    "method" 		=> "RolesController@update", 
                    "name"			=> "update", 
                    "id"			=> $row->idrole, 
                    "classes" 		=> "btn-icon btn-success update", 
                    "title" 		=> "Modificar",
                    "attributes" 	=> "",
                    "icon"          => "flaticon-edit",
                    "link" 			=> FALSE
                ],
                [
                    "method" 		=> "RolesController@permission_menu", 
                    "name"			=> "assing-permissions-menu", 
                    "id"			=> $row->idrole, 
                    "classes" 		=> "btn-icon btn-primary assing-permissions-menu", 
                    "title" 		=> "Asignar menú",
                    "attributes" 	=> "",
                    "icon"          => "fas fa-bars",
                    "link" 			=> FALSE
                ],
                [
                    "method" 		=> "RolesController@permission", 
                    "name"			=> "assing-permissions", 
                    "id"			=> $row->idrole, 
                    "classes" 		=> "btn-icon btn-danger assing-permissions", 
                    "title" 		=> "Asignar permiso",
                    "attributes" 	=> "",
                    "icon"          => "fas fa-eye",
                    "link" 			=> FALSE
                ],
            ];

            $buttons = botonera($function);

            $sub_array[] = $buttons; 

            $datas[] = $sub_array;                
        }

        $output = array(
            "draw"              =>  intval( $input["draw"] ),
            "recordsTotal"      =>  $filtered_rows,
            "recordsFiltered"   =>  $countRoles,
            "data"              =>  $datas
        );

        echo json_encode($output);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RolesCreateRequest $request)
    {
        $input = $request->all();

        $validator = $request->validated();

        $role = Roles::create($validator);
        
        $role->save();

        if(empty($role)){
            $msg[0][0] = "El rol no se ingreso registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }
            
        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El rol fue ingresado correctamente" );
        return response()->json($data, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $role = Roles::find($id);

        $permissionMenu = PermissionMenu::join("menu", "menu.idmenu", "=", "permission_menu.idmenu")
                            ->where("permission_menu.idrole", $id)                            
                            ->get(array(
                                "permission_menu.idpermission_menu",
                                "permission_menu.idrole",
                                "permission_menu.idmenu",
                            ));

        $permission = PermissionRole::join("permission_tree", "permission_tree.idpermission_tree", "=", "permission_role.idpermission_tree")
                            ->where("permission_role.idrole", $id)
                            ->get(array(
                                "permission_role.idpermission_role",
                                "permission_role.idrole",
                                "permission_role.idpermission_tree",
                            ));

        $output = array();
		$output["idrole"] = $role->idrole;
		$output["name"] = $role->name;
		$output["main_route"] = $role->main_route;
        $output["permissionMenu"] = $permissionMenu;
        $output["permission"] = $permission;

        return response()->json($output, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RolesCreateRequest $request, $id)
    {
        $role = Roles::find($id);

        if(empty($role)){
            $msg[0][0] = "El rol ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $input = $request->all();

        $validator = $request->validated();

        $role = Roles::where("idrole", $id)->update($validator);

        if(empty($role)){
            $msg[0][0] = "El rol no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }
    
        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El rol fue modificado correctamente" );
        return response()->json($data, 200);
    }

    public function permission_menu(Request $request, $id)
    {   
        $role = Roles::find($id);

        if(empty($role)){
            $msg[0][0] = "El rol ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }
        
        $input = $request->all();

        $deletePermission = PermissionMenu::where("idrole", $id)->delete();
        
        for ($i=0; $i < count($input["permissions"]); $i++) {
            $idmenu = substr($input["permissions"][$i], 5);
            $menu = Menus::find($idmenu);

            if(empty($menu)){
                $msg[0][0] = "El permiso no se encuentra registrado." ;
                $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                return response()->json($data, 200);
            }
            
            $data = array(
                "idrole" => $id,
                "idmenu" => $idmenu
            );

            $createPermissionMenu = PermissionMenu::create($data);

            $createPermissionMenu->save();
            
            if(empty($createPermissionMenu)){
                $msg[0][0] = "El permiso no se creo registrado." ;
                $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                return response()->json($data, 200);
            }

        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El permiso se ingreso correctamente." );
        return response()->json($data, 200);
    }
    
    public function permission(Request $request, $id)
    {   
        $role = Roles::find($id);

        if(empty($role)){
            $msg[0][0] = "El rol ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }
        
        $input = $request->all();

        $deletePermission = PermissionRole::where("idrole", $id)->delete();
        
        for ($i=0; $i < count($input["permissions"]); $i++) {
            $idpermission = substr($input["permissions"][$i], 5);
            $permissionTree = PermissionTree::find($idpermission);

            if(empty($permissionTree)){
                $msg[0][0] = "El permiso no se encuentra registrado." ;
                $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                return response()->json($data, 200);
            }

            $data = array(
                "idrole" => $id,
                "idpermission_tree" => $idpermission
            );

            $createPermission = PermissionRole::create($data);

            $createPermission->save();
            
            if(empty($createPermission)){
                $msg[0][0] = "El permiso no se creo registrado." ;
                $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                return response()->json($data, 200);
            }

        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El permiso se ingreso correctamente." );
        return response()->json($data, 200);
    }
    
}
