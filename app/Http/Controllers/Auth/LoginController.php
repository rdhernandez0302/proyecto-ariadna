<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;

class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $credentials = [
            $fieldType => $input['username'],
            'password' => $input['password']
        ];

        if( ! Auth::validate($credentials)){
            return redirect()->route('login')->with('error','Estas credenciales no coinciden con nuestros registros.')->with('username', $input['username']);
        }

        $user = User::where([$fieldType => $input['username']])->first();

        if($user->status == config("global.USER_INACTIVE")){
            return redirect()->route('login')->with('error','El usuario no se encuentra activo')->with('username', $input['username']);
        }

        $remember=false;
        if(isset($input['remember'])){
            $remember = $input['remember'];
        }

        if(auth()->attempt(array($fieldType => $input['username'], 'password' => $input['password']), $remember  )){
            $routeMain = Auth::user()->MainRouteRole;
            return redirect()->intended(route($routeMain));
        }else{
            return redirect()->route('login')->with('error','Estas credenciales no coinciden con nuestros registros.')->with('username', $input['username']);
        }
    }
}
