<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\PermissionTree;

class RouteController extends Controller
{

    public function index(){

        $routeCollection = \Route::getRoutes();

        foreach ($routeCollection as $value) {

            $route = explode("\\", $value->getActionName()); 

            if(count($route) > 2) {

                $route = array_values(array_slice($route, -3));

                if($route[0] == "Controllers"){

                    $permissionTree = PermissionTree::where('route', $route[1])->first();
                    $status = config('global.PERMISSION_TREE_PRIVATE');
                    if($route[1] == 'Auth'){
                        $status = config('global.PERMISSION_TREE_PUBLIC');
                    }

                    if(empty($permissionTree)) {
                        $data = array("father" => 0, "route" => $route[1], "status" => $status);
                        $permissionTree = PermissionTree::create($data);                    
                        $permissionTree->save();
                    }

                    $permissionTreeData = PermissionTree::where('father', $permissionTree->idpermission_tree)->where("route", $route[2])->first();
                    if(empty($permissionTreeData)) {
                        $data = array("father" => $permissionTree->idpermission_tree, "route" => $route[2], "status" => $status);
                        $permissionTree = PermissionTree::create($data);                    
                        $permissionTree->save();
                    }
                    
                }
            }
        }
        print_r("Se ingreso correctamente");
    }
    
}
