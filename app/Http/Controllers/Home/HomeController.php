<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Documents;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $today = date('Y-m-d');
        $from = date("Y-m-d", strtotime($today."- 1 week")) . ' 00:00:00';
        $to = $today . " 23:59:29";

        $allDocuments = Documents::count();
        $todayDocuments = Documents::whereDate('created_at', '>=', $today .' 00:00:00')->count();
        $weeklyDocuments = Documents::whereBetween('created_at', [ $from, $to ])->count();

        $data = [
            "allDocuments" => $allDocuments,
            "todayDocuments" => $todayDocuments,
            "weeklyDocuments" => $weeklyDocuments,
        ];

        return view('home')->with($data);
    }
}
