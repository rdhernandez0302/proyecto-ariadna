<?php

namespace App\Http\Controllers\Permissions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PermissionTree;
use App\Http\Requests\Permissions\PermissionsCreateRequest;

class PermissionsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $function = [
            [
                "method" 		=> "PermissionsController@store", 
                "name"			=> "create",  
                "classes" 		=> "btn-primary create", 
                "title" 		=> "Crear",
                "attributes" 	=> "",
                "icon"          => "fa fa-plus",
                "text"          => "Crear",
                "link" 			=> FALSE
            ]
        ];

        $buttons = botonera($function);

        $permissions= PermissionTree::all();
        $data = [
            "permissions"  => $permissions,
            "buttons" => $buttons
        ];
        
        return view("permissions.index")->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {   

        $input = $request->all();
        
        $permissions = PermissionTree::ListByFilters($input);
        $countPermissions = PermissionTree::CountListByFilters($input);

        $datas =  array();
        $filtered_rows = count($permissions);
        foreach($permissions as $row)
        {
            $father = "Principal";

            if( $row->father != 0){
                $permissionsFather = PermissionTree::find($row->father);
                if( ! empty($permissionsFather)){
                    $father = $permissionsFather->route;
                }
            }

            $status = "Protegido";
            $classStatus = "warning";
         
            if($row->status == config("global.PERMISSION_TREE_PUBLIC") ){
                $status = "Público";
                $classStatus = "success";
            }else if($row->status == config("global.PERMISSION_TREE_PRIVATE") ){
                $status = "Privado";
                $classStatus = "danger";
            }

            $sub_array = array();
            $sub_array[] = $row->idpermission_tree;
            $sub_array[] = $row->route;
            $sub_array[] = "<span class='label font-weight-bold label-lg label-light-".$classStatus." label-inline'>".$status."</span>";
            ;
            $sub_array[] = $father;
            
            $function = [
                [
                    "method" 		=> "PermissionsController@update", 
                    "name"			=> "update", 
                    "id"			=> $row->idpermission_tree, 
                    "classes" 		=> "btn-icon btn-success update", 
                    "title" 		=> "Modificar",
                    "attributes" 	=> "",
                    "icon"          => "flaticon-edit",
                    "link" 			=> FALSE
                ],
            ];

            $buttons = botonera($function);

            $sub_array[] = $buttons; 

            $datas[] = $sub_array;                
        }

        $output = array(
            "draw"              =>  intval( $input["draw"] ),
            "recordsTotal"      =>  $filtered_rows,
            "recordsFiltered"   =>  $countPermissions,
            "data"              =>  $datas
        );

        echo json_encode($output);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionsCreateRequest $request)
    {
        $input = $request->all();

        $validator = $request->validated();

        $permission = PermissionTree::create($validator);
        
        $permission->save();

        if(empty($permission)){
            $msg[0][0] = "El permiso no se ingreso registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }
            
        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El permiso fue ingresado correctamente" );
        return response()->json($data, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = PermissionTree::find($id);
        return response()->json($permission, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionsCreateRequest $request, $id)
    {
        $permission = PermissionTree::find($id);

        if(empty($permission)){
            $msg[0][0] = "El permiso ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $input = $request->all();

        $validator = $request->validated();
        $permission = PermissionTree::where("idpermission_tree", $id)->update($validator);
        
        if(empty($permission)){
            $msg[0][0] = "El permiso no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El permiso fue modificado correctamente" );
        return response()->json($data, 200);
    }
}
