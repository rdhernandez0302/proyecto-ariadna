<?php

namespace App\Http\Controllers\Menus;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menus;
use App\Http\Requests\Menus\MenusCreateRequest;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $function = [
            [
                "method" 		=> "MenusController@store", 
                "name"			=> "create",  
                "classes" 		=> "btn-primary create", 
                "title" 		=> "Crear",
                "attributes" 	=> "",
                "icon"          => "fa fa-plus",
                "text"          => "Crear",
                "link" 			=> FALSE
            ]
        ];

        $buttons = botonera($function);

        $menus= Menus::all();
        $data = [
            "menus"  => $menus,
            "buttons" => $buttons
        ];
        
        return view("menus.index")->with($data);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {   

        $input = $request->all();
        
        $menus = Menus::ListByFilters($input);
        $countMenus = Menus::CountListByFilters($input);

        $datas =  array();
        $filtered_rows = count($menus);
        foreach($menus as $row)
        {
            $father = "Principal";

            if( $row->father != 0){
                $menusFather = Menus::find($row->father);
                if( ! empty($menusFather)){
                    $father = $menusFather->name;
                }
            }

            $sub_array = array();
            $sub_array[] = $row->idmenu;
            $sub_array[] = $row->route;
            $sub_array[] = $row->name;
            $sub_array[] = $row->class;
            $sub_array[] = $row->icon;
            $sub_array[] = $father;
            
            $function = [
                [
                    "method" 		=> "MenusController@update", 
                    "name"			=> "update", 
                    "id"			=> $row->idmenu, 
                    "classes" 		=> "btn-icon btn-success update", 
                    "title" 		=> "Modificar",
                    "attributes" 	=> "",
                    "icon"          => "flaticon-edit",
                    "link" 			=> FALSE
                ],
            ];

            $buttons = botonera($function);

            $sub_array[] = $buttons; 

            $datas[] = $sub_array;                
        }

        $output = array(
            "draw"              =>  intval( $input["draw"] ),
            "recordsTotal"      =>  $filtered_rows,
            "recordsFiltered"   =>  $countMenus,
            "data"              =>  $datas
        );

        echo json_encode($output);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenusCreateRequest $request)
    {
        $input = $request->all();

        $validator = $request->validated();

        $menu = Menus::create($validator);
        
        $menu->save();

        if(empty($menu)){
            $msg[0][0] = "El menú no se ingreso registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }
            
        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El menú fue ingresado correctamente" );
        return response()->json($data, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menus::find($id);
        return response()->json($menu, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenusCreateRequest $request, $id)
    {
        $menu = Menus::find($id);

        if(empty($menu)){
            $msg[0][0] = "El menú ingresado no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $input = $request->all();

        $validator = $request->validated();

        $menu = Menus::where("idmenu", $id)->update($validator);

        if(empty($menu)){
            $msg[0][0] = "El menú no se modifico registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }
    
        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El menú fue modificado correctamente" );
        return response()->json($data, 200);
    }
}
