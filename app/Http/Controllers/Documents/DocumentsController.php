<?php

namespace App\Http\Controllers\Documents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Documents;
use App\Models\Themes;
use App\Models\ThemeDocument;
use App\Http\Requests\Documents\DocumentsCreateRequest;
use DB;


class DocumentsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $function = [
            [
                "method" 		=> "DocumentsController@store",
                "name"			=> "create",
                "classes" 		=> "btn-primary create",
                "title" 		=> "Crear",
                "attributes" 	=> "",
                "icon"          => "fa fa-plus",
                "text"          => "Crear",
                "link" 			=> FALSE
            ]
        ];

        $buttons = botonera($function);
        $themes = Themes::all();
        $data = [
            "buttons" => $buttons,
            "themes" => $themes
        ];

        return view("document.index")->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentsCreateRequest $request)
    {
        $input = $request->all();
        $validator = $request->validated();

        if( ! validateDate($validator["date"], "Y-m-d H:i") ) {
            $msg[0][0] = "Ingrese una fecha valida." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        if( ! empty($validator["themes"]) ){
            $themes = $validator["themes"];
            unset($validator["themes"]);
        }

        $validator["date"] =  date('Y-m-d H:i:s', strtotime($validator["date"]));

        $currentDate = strtotime($validator["date"]);
        $startDate = strtotime("2007-01-01");
        $endDate = strtotime("2013-12-31");

        if($startDate > $currentDate) {
            $msg[0][0] = "Ingrese una fecha mayor al año 2007." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        if($endDate < $currentDate) {
            $msg[0][0] = "Ingrese una fecha menor al año 2013." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $listDocument = Documents::where("number", $validator["number"])->first();

        if( ! empty($listDocument)){
            $msg[0][0] = "El número de radicado ya se encuentra registrado" ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        DB::beginTransaction();

        $document = Documents::create($validator);
        $document->save();

        if(empty($document)){
            DB::rollback();
            $msg[0][0] = "El radicado no se ingreso correctamente." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        if( ! empty($themes)){

            for ($i=0; $i < count($themes); $i++) {

                $dataDocumentTheme = [
                    'idtheme' => $themes[$i],
                    'iddocument' => $document->iddocument
                ];

                $documentThemes = ThemeDocument::create($dataDocumentTheme);
                $documentThemes->save();

                if(empty($documentThemes)){
                    DB::rollback();
                    $msg[0][0] = "El radicado no se ingreso correctamente." ;
                    $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
                    return response()->json($data, 200);
                }
            }
        }

        DB::commit();
        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El radicado fue ingresado correctamente" );
        return response()->json($data, 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $number
     * @return \Illuminate\Http\Response
     */
    public function show($number)
    {
        if( ! is_numeric($number)){
            $msg[0][0] = "Por favor ingrese solo números" ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $documentList = Documents::select('document.*', 'user.name AS nameUser')
                    ->join('user', 'user.iduser', '=', 'document.iduser_create')
                    ->where("number", $number)->first();

        if( empty($documentList)){
            $msg[0][0] = "No se encontraron registros con el número de radicado" ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $date = formatDate( date('Y-m-d', strtotime($documentList->date) ) );
        $date = $date["dia"] . " de " . $date["mes"] . " de " . $date["ano"]  . " - " . date('h:i A', strtotime($documentList->date));

        $documentList["title"] = ucfirst($documentList->title);
        $documentList["date"] = $date;


        $themes = Themes::select('theme.name')
                                ->join('theme_document', 'theme_document.idtheme', '=', 'theme.idtheme')
                                ->where("theme_document.iddocument", $documentList->iddocument)
                                ->get();

        $documentList["themes"] = $themes;

        return response()->json($documentList, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(empty($id)){
            $msg[0][0] = "El radicado ingresada no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $listDocument = Documents::find($id);

        if( empty($listDocument)){
            $msg[0][0] = "El radicado ingresada no se encuentra registrado." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            return response()->json($data, 200);
        }

        $deleteDocument = Documents::destroy($id);

        if(empty($deleteDocument)){
            $msg[0][0] = "El document no se elimino correctamente." ;
            $data = array( "res" => config("global.EXIT_ERROR"), "msg" => $msg);
            DB::rollback();
            return response()->json($data, 200);
        }

        $data = array( "res" => config("global.EXIT_SUCCESS"), "msg" => "El documento fue eliminada correctamente" );
        return response()->json($data, 200);

    }
}
