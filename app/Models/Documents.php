<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Documents extends Model
{

    protected $table = 'document';

    protected $primaryKey = 'iddocument';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'date', 'title'
    ];

    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->iduser_create = Auth::user()->iduser;
        });
    }
}
