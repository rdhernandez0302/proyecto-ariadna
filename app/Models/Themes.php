<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Themes extends Model
{
    protected $table = 'theme';

    protected $primaryKey = 'idtheme';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public $timestamps = true;

    public function scopeListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $search = $input["search"]["value"];
            $query->where(function($query) use ($search) {
                $query->where('name', 'like', '%'. $search .'%');
            });
        }

        if(isset($input["order"]) ) {

            $order = "idtheme";
            if($input["order"]==0){
                $order = "idtheme";
            }
            if($input["order"]==1){
                $order = "name";
            }

            $query->orderBy($order, $input["order"]["0"]["dir"]);
        }else{
            $query->orderBy("idtheme", "ASC");
        }

        if($input["length"] != "-1" ) {
            $query->offset($input["start"])->limit($input["length"]);
        }

        return $query->get();
    }

    public function scopeCountListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $query->where('name', 'like', '%'. $input["search"]["value"] . '%');
        }

        return $query->count();
    }
}
