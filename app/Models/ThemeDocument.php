<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThemeDocument extends Model
{
    protected $table = 'theme_document';

    protected $primaryKey = 'idtheme_document';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['idtheme', 'iddocument'];

    public $timestamps = true;
}
