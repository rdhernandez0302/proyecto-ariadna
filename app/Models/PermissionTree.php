<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionTree extends Model
{
    protected $table = 'permission_tree';

    protected $primaryKey = 'idpermission_tree';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'route', 'father', 'status'
    ];

    public $timestamps = true;

    public function scopeListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $query->where('route', 'like', '%'. $input["search"]["value"] . '%');
        }

        if(isset($input["order"][0]) ) {
            
            $order = "idpermission_tree";
            if($input["order"][0]["column"]==0){
                $order = "idpermission_tree";
            }
            if($input["order"][0]["column"]==1){
                $order = "route";
            }
            if($input["order"][0]["column"]==2){
                $order = "status";
            }
            if($input["order"][0]["column"]==3){
                $order = "father";
            }

            $query->orderBy($order, $input["order"]["0"]["dir"]);
        }else{
            $query->orderBy("idpermission_tree", "ASC");
        }

        if($input["length"] != "-1" ) {
            $query->offset($input["start"])->limit($input["length"]);
        }
        
        return $query->get();
    }

    public function scopeCountListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $query->where('route', 'like', '%'. $input["search"]["value"] . '%');
        }
        
        return $query->count();
    }

    public function scopeHasPermissionByParent($query, $data){
    
        $query->join('permission_role', 'permission_role.idpermission_tree', '=', 'permission_tree.idpermission_tree')
                    ->where("permission_tree.route", $data["route"])
                    ->where("permission_role.idrole", $data["idrole"])
                    ->get(array(
                        'permission_tree.*',
                    ));

        return $query->get();
    }
}
