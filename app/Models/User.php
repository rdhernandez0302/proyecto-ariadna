<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Roles;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

    protected $primaryKey = 'iduser';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'idrole', 'status', 'profile'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameRoleAttribute()
    {
        $role = Roles::find(Auth::user()->idrole);
        return $role->name;
    }

    public function getMainRouteRoleAttribute()
    {
        $role = Roles::find(Auth::user()->idrole);
        return $role->main_route;
    }

    public function scopeListByFilters($query, $input){

        $query->join('role', 'role.idrole', '=', 'user.idrole');

        $query->where('iduser', '!=', Auth::user()->iduser);

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $search = $input["search"]["value"];
            $query->where(function($query) use ($search) {
                $query->where('role.name', 'like', '%'. $search .'%');
                $query->orWhere('user.name', 'like', '%'. $search .'%');
                $query->orWhere('email', 'like', '%'. $search .'%');
                $query->orWhere('username', 'like', '%'. $search .'%');
            });
        }

        if(isset($input["order"][0]) ) {
            
            $order = "iduser";
            if($input["order"][0]["column"]==0){
                $order = "iduser";
            }
            if($input["order"][0]["column"]==1){
                $order = "name";
            }
            if($input["order"][0]["column"]==2){
                $order = "email";
            }
            if($input["order"][0]["column"]==3){
                $order = "username";
            }
            if($input["order"][0]["column"]==4){
                $order = "idrole";
            }
            if($input["order"][0]["column"]==5){
                $order = "status";
            }

            $query->orderBy($order, $input["order"]["0"]["dir"]);
        }else{
            $query->orderBy("iduser", "ASC");
        }

        if($input["length"] != "-1" ) {
            $query->offset($input["start"])->limit($input["length"]);
        }
        
        return $query->get(array(
            "user.*",
            "role.name AS name_rol"
        ));
    }

    public function scopeCountListByFilters($query, $input){
        
        $query->join('role', 'role.idrole', '=', 'user.idrole');

        $query->where('iduser', '!=', Auth::user()->iduser);

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $search = $input["search"]["value"];
            $query->where(function($query) use ($search) {
                $query->where('role.name', 'like', '%'. $search .'%');
                $query->orWhere('user.name', 'like', '%'. $search .'%');
                $query->orWhere('email', 'like', '%'. $search .'%');
                $query->orWhere('username', 'like', '%'. $search .'%');
            });
        }
        
        return $query->count();
    }
}
