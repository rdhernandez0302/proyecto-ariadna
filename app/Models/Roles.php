<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{

    protected $table = 'role';

    protected $primaryKey = 'idrole';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'main_route'
    ];

    public $timestamps = true;

    public function scopeListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $query->where('name', 'like', '%'. $input["search"]["value"] . '%');
        }

        if(isset($input["order"][0]) ) {
            
            $order = "idrole";
            if($input["order"][0]["column"]==0){
                $order = "idrole";
            }
            if($input["order"][0]["column"]==1){
                $order = "name";
            }

            $query->orderBy($order, $input["order"]["0"]["dir"]);
        }else{
            $query->orderBy("idrole", "ASC");
        }

        if($input["length"] != "-1" ) {
            $query->offset($input["start"])->limit($input["length"]);
        }
        
        return $query->get();
    }

    public function scopeCountListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $query->where('name', 'like', '%'. $input["search"]["value"] . '%');
        }
        
        return $query->count();
    }

}
