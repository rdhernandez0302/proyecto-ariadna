<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionMenu extends Model
{
    protected $table = 'permission_menu';

    protected $primaryKey = 'idpermission_menu';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idmenu', 'idrole'
    ];

    public $timestamps = true;

    public function menus()
    {
        return $this->belongsToMany('App\Menus', 'permission_menu', 'idmenu', 'idmenua');
    }
}
