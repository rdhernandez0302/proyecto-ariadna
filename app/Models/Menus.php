<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    protected $table = 'menu';

    protected $primaryKey = 'idmenu';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'route', 'father', 'name', 'class', 'icon'
    ];

    public $timestamps = true;

    public function scopeListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $search = $input["search"]["value"];
            $query->where(function($query) use ($search) {
                $query->where('name', 'like', '%'. $search .'%');
                $query->orWhere('class', 'like', '%'. $search .'%');
                $query->orWhere('route', 'like', '%'. $search .'%');
            });
        }

        if(isset($input["order"][0]) ) {
            
            $order = "idmenu";
            if($input["order"][0]["column"]==0){
                $order = "idmenu";
            }
            if($input["order"][0]["column"]==1){
                $order = "name";
            }
            if($input["order"][0]["column"]==3){
                $order = "class";
            }
            if($input["order"][0]["column"]==4){
                $order = "icon";
            }
            if($input["order"][0]["column"]==5){
                $order = "father";
            }

            $query->orderBy($order, $input["order"]["0"]["dir"]);
        }else{
            $query->orderBy("idmenu", "ASC");
        }

        if($input["length"] != "-1" ) {
            $query->offset($input["start"])->limit($input["length"]);
        }
        
        return $query->get();
    }

    public function scopeCountListByFilters($query, $input){

        if(isset($input["search"]["value"]) && ! empty($input["search"]["value"])) {
            $search = $input["search"]["value"];
            $query->where(function($query) use ($search) {
                $query->where('name', 'like', '%'. $search .'%');
                $query->orWhere('class', 'like', '%'. $search .'%');
                $query->orWhere('route', 'like', '%'. $search .'%');
            });
        }
        
        return $query->count();
    }

    public function scopeHasPermissionMenuByParent($query, $data){
    
        $query->join('permission_menu', 'permission_menu.idmenu', '=', 'menu.idmenu')
                    ->where("menu.father", $data["father"])
                    ->where("permission_menu.idrole", $data["idrole"])
                    ->get(array(
                        'menu.*',
                    ));

                
        return $query->get();
    }

}
