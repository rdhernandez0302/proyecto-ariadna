## Configuración

- ###### Agregar el archivo ".env", modificando los datos de accesos a la BD:

    <pre>
    APP_NAME='Prueba ariadna'
    APP_ENV=local
    APP_KEY=base64:J8DTWe7tJQEBEhgJUYiGe5tEwVx5hk9A05EwTRY+Qbg=
    APP_DEBUG=true
    APP_URL=http://localhost

    LOG_CHANNEL=stack

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=prueba_ariadna
    DB_USERNAME=root
    DB_PASSWORD=

    BROADCAST_DRIVER=log
    CACHE_DRIVER=file
    QUEUE_CONNECTION=sync
    SESSION_DRIVER=file
    SESSION_LIFETIME=120

    REDIS_HOST=127.0.0.1
    REDIS_PASSWORD=null
    REDIS_PORT=6379

    MAIL_DRIVER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null

    AWS_ACCESS_KEY_ID=
    AWS_SECRET_ACCESS_KEY=
    AWS_DEFAULT_REGION=us-east-1
    AWS_BUCKET=

    PUSHER_APP_ID=
    PUSHER_APP_KEY=
    PUSHER_APP_SECRET=
    PUSHER_APP_CLUSTER=mt1

    MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
    MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

    JWT_SECRET=26BvG4GblTEmKjeMRUwYihlq5bA5zQgTLgmnNha686lzdsbOVTrvnkElHXvdU6Xj
    </pre>

- ###### Instalar composer

    #### Iniciamos la consola administrativa, nos dirigimos hasta la ruta raiz y agregamos el siguiente comando
    <pre>composer install</pre>
 
- ###### Generar base de datos

    #### En la carpeta raiz existe un archivo llamado bd-prueba-ariadna-01-02-2021.sql, ejecutarlo para migrar las rutas y los usuarios respectivos. 
    <pre>Usuario: admin</pre>
    <pre>Contraseña: 123456789</pre>

- ###### Inicializar la aplicación
    #### una vez configurado todo lo anterior procedemos a iniciar el servidor
    <pre>php artisan serve</pre>
