<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_role', function (Blueprint $table) {
            $table->id('idpermission_role');
            $table->unsignedBigInteger('idpermission_tree');
            $table->foreign('idpermission_tree')->references('idpermission_tree')->on('permission_tree');
            $table->unsignedBigInteger('idrole');
            $table->foreign('idrole')->references('idrole')->on('role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_role');
    }
}
