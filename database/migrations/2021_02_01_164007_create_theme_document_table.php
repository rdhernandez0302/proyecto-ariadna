<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemeDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theme_document', function (Blueprint $table) {
            $table->id('idtheme_document');
            $table->unsignedBigInteger('iddocument');
            $table->foreign('iddocument')->references('iddocument')->on('document')->onDelete('cascade');
            $table->unsignedBigInteger('idtheme');
            $table->foreign('idtheme')->references('idtheme')->on('theme')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theme_document');
    }
}
