<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_menu', function (Blueprint $table) {
            $table->id('idpermission_menu');
            $table->unsignedBigInteger('idmenu');
            $table->foreign('idmenu')->references('idmenu')->on('menu');
            $table->unsignedBigInteger('idrole');
            $table->foreign('idrole')->references('idrole')->on('role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_menu');
    }
}
