<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert([
            'name' => 'Administrador',
            'main_route' => 'users.index',
        ]);

        DB::table('user')->insert([
            'name' => 'Ronald Hernandez',
            'username' => 'rdhernandez0302',
            'email' => 'rdhernandez0302@gmail.com',
            'password' => Hash::make('123456789'),
            'idrole' => 1,
        ]);
    }
}
