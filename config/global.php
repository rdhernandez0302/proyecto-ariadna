<?php

return [
    'EXIT_ERROR' => 0,
    'EXIT_SUCCESS' => 1,
    'PERMISSION_TREE_PUBLIC' => 1,
    'PERMISSION_TREE_PRIVATE' => 2,
    'PERMISSION_TREE_PROTECTED' => 3,
    'USER_ACTIVE' => 1,
    'USER_INACTIVE' => 0,
]

?>