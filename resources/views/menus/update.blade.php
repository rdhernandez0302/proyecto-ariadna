<div class="modal fade" id="menuModalUpdate" tabindex="-1" role="dialog" aria-labelledby="menuModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_update" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="menuModalLabel">Modificar menú</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
       
                    <div class="form-group">
                        <label>Ruta</label>
                        <input type="text" name="route_update" id="route_update" class="form-control required"  placeholder="Ingrese la ruta"/>
                    </div>

                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="name_update" id="name_update" class="form-control required"  placeholder="Ingrese el nombre"/>
                    </div>

                    <div class="form-group">
                        <label>Clase</label>
                        <input type="text" name="class_update" id="class_update" class="form-control required"  placeholder="Ingrese la clase"/>
                    </div>

                    <div class="form-group">
                        <label>Icono <span class="text-muted"> - opcional</span></label>
                        <input type="text" name="icon_update" id="icon_update" class="form-control"  placeholder="Ingrese el icono"/>
                    </div>

                    <div class="form-group">
                        <label>Padre <span class="text-muted"> - opcional</span></label>
                        
                        <select class="form-control" id="father_update" name="father_update">
                        
                            <option value="0">Seleccione el padre</option>

                            @foreach ($menus as $menu)
                                <option value="{{ $menu->idmenu }}">{{ $menu->name }}</option>
                            @endforeach
                            
                        </select>
                        
                        <span class="form-text text-muted">Si desea crear un submenú seleccione este campo, de lo contrario déjelo vacío.</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_menu_update" id="id_menu_update" />
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success font-weight-bold">Modificar menú</button>
                </div>
            </div>
        </form>
    </div>
</div>