@extends('layouts.admin.admin')

@section('title')
    Menús
@endsection

@section('content')

<div class="card card-custom">
	<div class="card-header flex-wrap py-5">
		<div class="card-title">
			<h3 class="card-label">
                Menús   
				<span class="d-block text-muted pt-2 font-size-sm">Lista de menús registrados</span>
            </h3>
        </div>
        <div class="card-toolbar">
            {!! $buttons !!}
		</div>
       
	</div>
	<div class="card-body">
        <div class="dataTables_wrapper dt-bootstrap4 no-footer">

            <table id="menus" class="table table-separate table-checkable dataTable no-footer dtr-inline" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ruta</th>
                        <th>Nombre</th>
                        <th>Clase</th>
                        <th>Icono</th>
                        <th>Padre</th>
                        <th>Acción</th>
                    </tr>
                </thead>
            </table>
  
        </div>
	</div>
</div>


@include('menus.create')
@include('menus.update')

@endsection

@section('script')
    <script src="{{ asset('js/menus.js') }}"></script>
@endsection
