@extends('layouts.admin.admin')

@section('title')
    Inicio
@endsection

@section('content')

<div class="row">
    <div class="col-xl-4">
        <a href="#" class="card card-custom bg-danger bg-hover-state-danger card-stretch gutter-b">
            <div class="card-body">
                <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                    <i class="flaticon2-list-3" style="font-size: 30px; color:#Fff"></i>
                </span>
                <div class="text-inverse-danger font-weight-bolder font-size-h5 mb-2 mt-5">{{$todayDocuments}}</div>
                <div class="font-weight-bold text-inverse-danger font-size-sm">Radicados creados hoy</div>
            </div>
        </a>
    </div>
    <div class="col-xl-4">
        <a href="#" class="card card-custom bg-primary bg-hover-state-primary card-stretch gutter-b">
            <div class="card-body">
                <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                    <i class="flaticon2-list-3" style="font-size: 30px; color:#Fff"></i>
                </span>
                <div class="text-inverse-primary font-weight-bolder font-size-h5 mb-2 mt-5">{{$weeklyDocuments}}</div>
                <div class="font-weight-bold text-inverse-primary font-size-sm">Radicados creados la semana pasada</div>
            </div>
        </a>
    </div>
    <div class="col-xl-4">
        <a href="#" class="card card-custom bg-success bg-hover-state-success card-stretch gutter-b">
            <div class="card-body">
                <span class="svg-icon svg-icon-white svg-icon-3x ml-n1">
                    <i class="flaticon2-list-3" style="font-size: 30px; color:#Fff"></i>
                </span>
                <div class="text-inverse-success font-weight-bolder font-size-h5 mb-2 mt-5">{{$allDocuments}}</div>
                <div class="font-weight-bold text-inverse-success font-size-sm">Total de radicados</div>
            </div>
        </a>
    </div>
</div>

@endsection

@section('script')
    <script>
        $(".parameters-menu").addClass("menu-item-open");
        $(".home-menu").addClass("menu-item-active");
    </script>
@endsection
