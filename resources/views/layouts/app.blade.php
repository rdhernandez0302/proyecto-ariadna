<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <title>@yield('title') | {{ config('app.name', 'Home') }}</title>
    <meta name="description" content="Base form control examples" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    <link href="{{ asset('css/template/login-2.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/general.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/template/base/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/menu/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/brand/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/aside/light.css') }}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="favicon.ico" />

</head>

<body  id="kt_body"  class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading"  >
    <div class="login login-2 login-signin-on d-flex flex-column flex-lg-row flex-row-fluid bg-white" id="kt_login">
        <!--begin::Aside-->
        <div class="login-aside order-2 order-lg-1 d-flex flex-column-fluid flex-lg-row-auto bgi-size-cover bgi-no-repeat p-7 p-lg-10">
            <!--begin: Aside Container-->
            <div class="d-flex flex-row-fluid flex-column justify-content-between">
                <!--begin::Aside body-->
                <div class="d-flex flex-column-fluid flex-column flex-center mt-5 mt-lg-0">
                    <a href="#" class="mb-15 text-center">
                        <img src="https://www.ariadnacommunicationsgroup.com/assets/images/logotype.svg" class="max-h-75px max-w-350px" alt="Logo"/>
                    </a>
                <!--end::Aside body-->
                @yield('content')
                </div>
            <!--end::Aside body-->

            <!--begin: Aside footer for desktop-->
            <div class="d-flex flex-column-auto justify-content-between mt-15">
                <div class="text-dark-50 font-weight-bold order-2 order-sm-1 my-2">
                    &copy; {{ date('Y') }}
                </div>

            </div>
            <!--end: Aside footer for desktop-->
        </div>
        <!--end: Aside Container-->
    </div>
    <!--begin::Aside-->

    <!--begin::Content-->
    <div class="order-1 order-lg-2 flex-column-auto flex-lg-row-fluid d-flex flex-column p-7" style="background-image: url({{ asset('img/background-login.jpg') }});">
        <!--begin::Content body-->
        <div class="d-flex flex-column-fluid flex-lg-center">
            <div class="d-flex flex-column justify-content-center">
                <h3 class="display-3 font-weight-bold my-7 text-white">¡Bienvenido!</h3>
                <p class="font-weight-bold font-size-lg text-white opacity-80">
                    PRUEBA ARIADNA RONALD HERNANDEZ
                </p>
            </div>
        </div>
        <!--end::Content body-->
    </div>
    <!--end::Content-->
</div>


    <script src="{{ asset('js/template/plugins.bundle.js') }}"></script>
    <script src="{{ asset('js/template/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('js/template/scripts.bundle.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validate/jquery.validate.defaults.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validate/localization/messages_es.js') }}"></script>
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/plugins/scripts.js') }}"></script>
    <script src="{{ asset('js/login.js') }}"></script>

    @yield('script')
</body>

</html>
