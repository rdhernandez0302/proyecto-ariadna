<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <title>@yield('title') | {{ config('app.name', 'Home') }}</title>
    <meta name="description" content="Base form control examples" />    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    <link href="{{ asset('css/template/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/template/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/base/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/menu/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/brand/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/aside/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/general.css') }}" rel="stylesheet" type="text/css" />
    
    <link rel="shortcut icon" href="favicon.ico" />

    @yield('style')
</head>

<body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">

    @include('layouts.admin.mobile')
    
    <!--end::Header Mobile-->
    <div class="d-flex flex-column flex-root">
        <!--begin::Page-->
        <div class="d-flex flex-row flex-column-fluid page">

            @include('layouts.admin.aside')

            <!--begin::Wrapper-->
            <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
                
                @include('layouts.admin.header')

                <!--begin::Content-->
                <div class="content  d-flex flex-column flex-column-fluid" id="kt_content">
                
                    <!--begin::Entry-->
                    <!-- <div class="d-flex flex-column-fluid"> -->
                    <div class="d-flex flex-column-fluid">
                        <div class="container-fluid">
                            @yield('content')
                        </div>
                    </div>
                    <!--end::Entry-->
                </div>
                <!--end::Content-->

                @include('layouts.admin.footer')
            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Page-->
    </div>
    <!--end::Main-->

    
    @include('layouts.admin.panel')

    <!--begin::Scrolltop-->
    <div id="kt_scrolltop" class="scrolltop">
        <span class="svg-icon"><!--begin::Svg Icon | path:/metronic/themes/metronic/theme/html/demo1/dist/assets/media/svg/icons/Navigation/Up-2.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <polygon points="0 0 24 0 24 24 0 24"/>
            <rect fill="#000000" opacity="0.3" x="11" y="10" width="2" height="10" rx="1"/>
            <path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero"/>
            </g>
            </svg><!--end::Svg Icon-->
        </span>
    </div>
    <!--end::Scrolltop-->

    <script>
        var base_url = '{{ url('/') }}/';
        var csrf_token = '{{ csrf_token() }}';
        var assets_url = '{{ asset('') }}';
    </script>                                        

    <script src="{{ asset('js/template/plugins.bundle.js') }}"></script>
    <script src="{{ asset('js/template/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('js/template/scripts.bundle.js') }}"></script>
    <script src="{{ asset('js/template/paginations.js') }}"></script>
    
    <script src="{{ asset('js/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validate/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validate/jquery.validate.defaults.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-validate/localization/messages_es.js') }}"></script>
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/datatables.bundle.js') }}"></script>

    <script src="{{ asset('js/plugins/bootstrap-4/bootstrap.bundle.min.js') }}"></script>                                        
    <script src="{{ asset('js/plugins/scripts.js') }}"></script>
    <script src="{{ asset('js/plugins/constants.js') }}"></script>
    
    @yield('script')
</body>

</html>