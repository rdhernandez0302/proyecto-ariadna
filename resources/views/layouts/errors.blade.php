<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name', 'Errores') }}</title>
    <meta name="description" content="Base form control examples" />    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

    <link href="{{ asset('css/template/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/template/error.css') }}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="favicon.ico" />

</head>

<body  id="kt_body"  class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading"  >
    @yield('content')
</body>
</html>