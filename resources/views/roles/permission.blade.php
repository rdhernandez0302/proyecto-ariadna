<div class="modal fade" id="permissionModal" tabindex="-1" role="dialog" aria-labelledby="permissionLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_permission" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="permissionMenuLabel">Permisos Ruta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                 
                    <div id="jstree" class="tree-demo">
                        @foreach ($treePermissions as $treePermission)
                            {!! $treePermission !!}
                        @endforeach
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_role_permission" id="id_role_permission" />
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success font-weight-bold">Modificar permisos ruta</button>
                </div>
            </div>
        </form>
    </div>
</div>