<div class="modal fade" id="permissionMenuModal" tabindex="-1" role="dialog" aria-labelledby="permissionMenuLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_permission_menu" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="permissionMenuLabel">Permisos menú</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
        
                    <div id="jstree_menu" class="tree-demo">
                        @foreach ($treePermissionMenu as $treePermission)
                            {!! $treePermission !!}
                        @endforeach
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_role_permission_menu" id="id_role_permission_menu" />
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success font-weight-bold">Modificar permisos menú</button>
                </div>
            </div>
        </form>
    </div>
</div>