<div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="roleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_create" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="roleModalLabel">Crear rol</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Rol</label>
                        <input type="text" name="name" id="name" class="form-control required"  placeholder="Ingrese el rol"/>
                    </div>

                    <div class="form-group">
                        <label>Ruta principal</label>
                        <input type="text" name="main_route" id="main_route" class="form-control required"  placeholder="Ingrese la ruta"/>
                        <span class="form-text text-muted">Ruta en la cual será dirigido el usuario al momento de iniciar sesión.</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Crear rol</button>
                </div>
            </div>
        </form>
    </div>
</div>