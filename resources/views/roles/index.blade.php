@extends('layouts.admin.admin')

@section('title')
    Roles
@endsection

@section('content')

<div class="card card-custom">
	<div class="card-header flex-wrap py-5">
		<div class="card-title">
			<h3 class="card-label">
				Roles
				<span class="d-block text-muted pt-2 font-size-sm">Lista de roles registrados</span>
            </h3>
        </div>
        <div class="card-toolbar">
            {!! $buttons !!}
		</div>
	</div>
	<div class="card-body">
        <div class="dataTables_wrapper dt-bootstrap4 no-footer">

            <table id="roles" class="table table-separate table-checkable dataTable no-footer dtr-inline" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Acción</th>
                    </tr>
                </thead>
            </table>
  
        </div>
	</div>
</div>


@include('roles.create')
@include('roles.update')
@include('roles.permission_menu')
@include('roles.permission')

@endsection

@section('script')
    <link href="{{ asset('css/plugins/jquery-tree/jstree.bundle.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ asset('js/plugins/jquery-tree/jstree.bundle.js') }}"></script>    
    <script src="{{ asset('js/plugins/jquery-tree/treeview.js') }}"></script>    
    <script src="{{ asset('js/roles.js') }}"></script>
@endsection
