<div class="modal fade" id="documentModal" tabindex="-1" role="dialog" aria-labelledby="documentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_create" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="documentModalLabel">Crear radicado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label>No. de radicado</label>
                        <input type="text" name="number-add" id="number-add" class="form-control required"  placeholder="Ingrese el no. de radicado"/>
                    </div>

                    <div class="form-group">
                        <label>Fecha</label>
                        <input type="text" autocomplete="off" name="date-add" id="date-add" class="form-control date-add required"  placeholder="Ingrese la fecha"/>
                    </div>

                    <div class="form-group">
                        <label>Título</label>
                        <input type="text" name="title-add" id="title-add" class="form-control required"  placeholder="Ingrese el título"/>
                    </div>

                    <div class="">
                        <label>Temas</label>
                        <select multiple="true" name="themes-add[]" id="themes-add" class="form-control required select2" style="width: 100%">
                            @foreach ($themes as $theme)
                                <option value="{{ $theme->idtheme }}">{{ $theme->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Crear radicado</button>
                </div>
            </div>
        </form>
    </div>
</div>
