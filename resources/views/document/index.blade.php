@extends('layouts.admin.admin')

@section('style')
    <link href="{{ asset('css/plugins/jquery-datetimepicker/jquery-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('title')
    Radicados
@endsection

@section('content')

<div class="card card-custom">
	<div class="card-header flex-wrap py-5">
		<div class="card-title">
			<h3 class="card-label">
                Radicados
				<span class="d-block text-muted pt-2 font-size-sm">Busca el radicado por su número</span>
            </h3>
        </div>
        <div class="card-toolbar">
            {!! $buttons !!}
		</div>

	</div>
	<div class="card-body">

        <div class="row justify-content-center mb-10">

            <div class="col-md-5">
                <div class="input-group">
                    <input type="text" name="input-search-document" id="input-search-document" class="form-control" placeholder="Buscar por número de radicado">
                    <div class="input-group-append">
                        <button class="btn btn-primary" name="btn-search-document" id="btn-search-document" type="button">Buscar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card card-custom overflow-hidden d-none card-document">
            <div class="card-body p-0">
                <div class="row justify-content-center py-8 px-8 px-md-0">
                        <div class="col-md-9">
                            <div class="d-flex justify-content-between flex-column flex-md-row">
                                <div class="d-flex flex-column align-items-md-start px-0">
                                    <h1 class="display-4 font-weight-boldest number">No</h1>
                                    <p class="mb-10 font-size-h5 title">titulo</p>
                                </div>
                                <div class="d-flex flex-column align-items-md-end px-0">
                                    <span class="d-flex flex-column align-items-md-end opacity-70">
                                        <span class="date">Fecha</span>
                                        <span class="created_at">Creado por: </span>

                                    </span>
                                </div>
                            </div>
                            <div class="border-bottom w-100"></div>
                            <div class="d-flex justify-content-between pt-6">
                                <div class="d-flex flex-column flex-root">
                                    <span class="font-weight-bolder mb-2">Temas</span>
                                    <div class="d-flex flex-column flex-root container-themes">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row justify-content-center py-8 px-8 px-md-0">
                        <div class="col-md-9">
                            <div class="d-flex justify-content-end">
                                <button type="button" class="btn btn-danger font-weight-bold btn-delete-document" >Eliminar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

	</div>
</div>

@include('document.create')

@endsection

@section('script')
    <script src="{{ asset('js/documents.js') }}"></script>
    <script src="{{ asset('js/plugins/jquery-datetimepicker/jquery-datetimepicker-full.min.js') }}"></script>
@endsection
