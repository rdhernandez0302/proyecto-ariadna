<div class="modal fade" id="permissionModal" tabindex="-1" role="dialog" aria-labelledby="permissionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_create" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="permissionModalLabel">Crear permiso</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Ruta</label>
                        <input type="text" name="route" id="route" class="form-control required"  placeholder="Ingrese la ruta"/>
                    </div>

                    <div class="form-group">
                        <label>Estado <span class="text-muted"></span></label>
                        
                        <select class="form-control required" id="status" name="status">
                            <option value="">Seleccione el estado</option>
                            <option value="{{ config('global.PERMISSION_TREE_PUBLIC') }}">Público</option>
                            <option value="{{ config('global.PERMISSION_TREE_PROTECTED') }}">Protegido</option>
                            <option value="{{ config('global.PERMISSION_TREE_PRIVATE') }}">Privado</option>
                        </select>
                
                    </div>

                    <div class="form-group">
                        <label>Padre <span class="text-muted"> - opcional</span></label>
                        
                        <select class="form-control" id="father" name="father">
                        
                            <option value="0">Seleccione el padre</option>
                            @foreach ($permissions as $permission)
                                <option value="{{ $permission->idpermission_tree }}">{{ $permission->route }}</option>
                            @endforeach
                        </select>
                    
                        <span class="form-text text-muted">Si desea crear un submenú seleccione este campo, de lo contrario déjelo vacío.</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Crear menú</button>
                </div>
            </div>
        </form>
    </div>
</div>