@extends('layouts.admin.admin')

@section('title')
    Permisos
@endsection

@section('content')

<div class="card card-custom">
	<div class="card-header flex-wrap py-5">
		<div class="card-title">
			<h3 class="card-label">
                Permisos   
				<span class="d-block text-muted pt-2 font-size-sm">Lista de permisos registrados</span>
            </h3>
        </div>
        <div class="card-toolbar">
            {!! $buttons !!}
		</div>
       
	</div>
	<div class="card-body">
        <div class="dataTables_wrapper dt-bootstrap4 no-footer">

            <table id="permissions" class="table table-separate table-checkable dataTable no-footer dtr-inline" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Ruta</th>
                        <th>Estado</th>
                        <th>Padre</th>
                        <th>Acción</th>
                    </tr>
                </thead>
            </table>
  
        </div>
	</div>
</div>


@include('permissions.create')
@include('permissions.update')

@endsection

@section('script')
    <script src="{{ asset('js/permissions.js') }}"></script>
@endsection
