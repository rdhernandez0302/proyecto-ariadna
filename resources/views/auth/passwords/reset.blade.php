@extends('layouts.app')

@section('title')
    Restablecer contraseña
@endsection

@section('content')

<!--begin::Signin-->
<div class="login-form login-signin">
    <div class="text-center mb-10 mb-lg-20">
        <h2 class="font-weight-bold">Restablecer contraseña</h2>
        <p class="text-muted font-weight-bold">Ingresa el usuario y contraseña</p>
    </div>

    <!--begin::Form-->
    <form class="form_reset_password_update" id="form_reset_password_update" method="POST" action="{{ route('password.update') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="form-group py-3 m-0">
            <input class="form-control h-auto border-0 px-0 placeholder-dark-75 required" type="email" placeholder="Email" name="email" id="email" autocomplete="off" value="{{ $email ?? old('email') }}"/>
        </div>
        <div class="form-group py-3 border-top m-0">
            <input class="form-control h-auto border-0 px-0 placeholder-dark-75 password" type="password" placeholder="Contraseña" id="new_password" name="password" autocomplete="off"/>
        </div>

        <div class="form-group py-3 border-top m-0">
            <input class="form-control h-auto border-0 px-0 placeholder-dark-75 password_repit" type="password" placeholder="Repetir contraseña" id="password-confirm" name="password_confirmation" autocomplete="off"/>
        </div>

        <div class="form-group d-flex flex-wrap flex-center">
            <button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3">Restablecer contraseña</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Signin-->
           
@endsection

@section('script')

    @error('email')
        <script>
            Swal.fire({
                title: '¡Alerta!', 
                icon: 'error',
                text: '{{ $message }}'
            });
        </script>
    @enderror

    @error('password')
        <script>
            Swal.fire({
                title: '¡Alerta!', 
                icon: 'error',
                text: '{{ $message }}'
            });
        </script>
    @enderror

@endsection
