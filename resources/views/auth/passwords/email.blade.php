@extends('layouts.app')

@section('title')
    Restablecer contraseña
@endsection

@section('content')

<div class="">
    <div class="text-center mb-10 mb-lg-20">
        <h3 class="">¿Contraseña olvidada?</h3>
        <p class="text-muted font-weight-bold">Ingrese su correo electrónico para restablecer su contraseña</p>
    </div>

    <form class="form_reset_password" id="form_reset_password" method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="form-group border-bottom">
            <input class="form-control h-auto border-0 px-0 placeholder-dark-75 required" type="email" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}" />
        </div>
        <div class="mb-10">

        </div>
        <div class="form-group d-flex flex-wrap flex-center">
            <button id="kt_login_forgot_submit" class="btn btn-primary  font-weight-bold px-9 py-4 my-3 mx-2">Recuperar</button>
            <a href="{{ route('login') }}" class="btn btn-light-primary font-weight-bold px-9 py-4 my-3 mx-2">Regresar</a>
        </div>
    </form>
</div>
           
@endsection

@section('script')

    @if (session('status'))
        <script>
            Swal.fire({
                title: '¡Éxito!', 
                icon: 'success',
                text: '{{ session('status') }}'
            });
        </script>
    @endif
    @error('email')
        <script>
            Swal.fire({
                title: '¡Alerta!', 
                icon: 'error',
                text: '{{ $message }}'
            });
        </script>
    @enderror

@endsection
