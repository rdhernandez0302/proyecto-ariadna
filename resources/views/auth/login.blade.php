@extends('layouts.app')

@section('title')
    Login
@endsection

@section('content')

<!--begin::Signin-->
<div class="login-form login-signin">
    <div class="text-center mb-10 mb-lg-20">
        <h2 class="font-weight-bold">Inicia sesión</h2>
        <p class="text-muted font-weight-bold">Ingresa el usuario y contraseña</p>
    </div>

    <!--begin::Form-->
    <form class="form_login" id="form_login" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group py-3 m-0">
            <input class="form-control h-auto border-0 px-0 placeholder-dark-75 required" type="text" placeholder="Nombre Usuario" name="username" id="username" autocomplete="off" value="{{ session('username') }}"/>
        </div>
        <div class="form-group py-3 border-top m-0">
            <input class="form-control h-auto border-0 px-0 placeholder-dark-75 required" type="password" placeholder="Contraseña" id="password" name="password" autocomplete="off"/>
        </div>

        <div class="form-group d-flex flex-wrap justify-content-between align-items-center mt-3">
            <div class="checkbox-inline">
                <label class="checkbox checkbox-outline m-0 text-muted">
                    <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
                    <span></span>
                    Recuérdame
                </label>
            </div>
            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}" id="kt_login_forgot" class="text-muted text-hover-primary">¿Se te olvidó tu contraseña?</a>
            @endif
        </div>

        <div class="form-group d-flex flex-wrap justify-content-between align-items-center mt-2">
            <div class="my-3 mr-2">
            </div>
            <button type="submit" id="kt_login_signin_submit" class="btn btn-primary font-weight-bold px-9 py-4 my-3">Iniciar sesión</button>
        </div>
    </form>
    <!--end::Form-->
</div>
<!--end::Signin-->
@if ($errors->has('login'))
<span class=”help-block”>
<strong>{{ $errors->first('login') }}</strong>
</span>
@endif   
@endsection

@section('script')

    @if (session('error'))
    
        <script>
            Swal.fire({
                title: '¡Alerta!', 
                icon: 'error',
                text: '{{ session('error') }}'
            });
        </script>
    @endif

@endsection
