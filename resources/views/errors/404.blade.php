@extends('layouts.errors')

@section('content') 
    
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
	<!--begin::Error-->
        <div class="error error-5 d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url(/metronic/themes/metronic/theme/html/demo1/dist/assets/media/error/bg5.jpg);">
        <!--begin::Content-->
        <div class="container d-flex flex-row-fluid flex-column justify-content-md-center p-12">
            <h1 class="error-title font-weight-boldest text-info mt-10 mt-md-0 mb-12">404!</h1>
            <p class="font-weight-boldest display-4">
                Algo salió mal aquí.
            </p>
            <p class="font-size-h3">
                Regresar al inicio haciendo clic <a href="/">aquí</a>
            </p>
        </div>
        <!--end::Content-->
    </div>
    <!--end::Error-->
</div>
<!--end::Main-->

@endsection


