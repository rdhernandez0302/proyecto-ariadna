<div class="modal fade" id="themeModalUpdate" tabindex="-1" role="dialog" aria-labelledby="themeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_update" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="themeModalLabel">Modificar tema</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="name_update" id="name_update" class="form-control required"  placeholder="Ingrese el nombre"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_theme_update" id="id_theme_update" />
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success font-weight-bold">Modificar tema</button>
                </div>
            </div>
        </form>
    </div>
</div>
