@extends('layouts.admin.admin')

@section('title')
    Temas
@endsection

@section('content')

<div class="card card-custom">
	<div class="card-header flex-wrap py-5">
		<div class="card-title">
			<h3 class="card-label">
                Temas
				<span class="d-block text-muted pt-2 font-size-sm">Lista de temas registrados</span>
            </h3>
        </div>
        <div class="card-toolbar">
            {!! $buttons !!}
		</div>

	</div>
	<div class="card-body">
        <div class="dataTables_wrapper dt-bootstrap4 no-footer">

            <table id="themes" class="table table-separate table-checkable dataTable no-footer dtr-inline" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Acción</th>
                    </tr>
                </thead>
            </table>

        </div>
	</div>
</div>

@include('themes.create')
@include('themes.update')

@endsection

@section('script')
    <script src="{{ asset('js/themes.js') }}"></script>
@endsection
