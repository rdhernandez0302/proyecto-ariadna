<div class="modal fade" id="userPasswordModal" tabindex="-1" role="dialog" aria-labelledby="userPasswordModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_password" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userPasswordModalLabel">Modificar contraseña</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nueva contraseña</label>
                        <input type="password" name="change_password" id="change_password" class="form-control password"  placeholder="Ingrese la contraseña" autocomplete="off"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_user_password" id="id_user_password" />
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Cambiar contraseña</button>
                </div>
            </div>
        </form>
    </div>
</div>