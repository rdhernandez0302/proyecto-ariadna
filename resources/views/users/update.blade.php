<div class="modal fade" id="userModalUpdate" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="" method="POST" id="form_update" onsubmit="return false">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userModalLabel">Modificar usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" name="name" id="name_update" class="form-control required"  placeholder="Ingrese el nombre"/>
                    </div>

                    <div class="form-group">
                        <label>Nombre Usuario</label>
                        <input type="text" name="username" id="username_update" class="form-control required"  placeholder="Ingrese el nombre usuario"/>
                    </div>

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" id="email_update" class="form-control email"  placeholder="Ingrese el nombre"/>
                    </div>

                    <div class="form-group">
                        <label>Rol</label>
                        
                        <select class="form-control" name="idrole" id="idrole_update" >
                            <option value="">Seleccione el rol</option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->idrole }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <br>
                        <div class="image-input image-input-empty image-input-outline" id="user_edit_update_avatar" style="background-image:url('{{ asset('img/profile_photo.png') }}')">
                            <div class="image-input-wrapper"></div>
                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                <i class="fa fa-pen icon-sm text-muted"></i>
                                <input type="file" name="profile" accept=".png, .jpg, .jpeg" />
                                <input type="hidden" name="profile_update_remove" />
                            </label>
                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow cancel-profile-update" data-action="cancel" data-toggle="tooltip" title="Cancelar perfil">
                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>
                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_user_update" id="id_user_update" />
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-success font-weight-bold">Modificar usuario</button>
                </div>
            </div>
        </form>
    </div>
</div>