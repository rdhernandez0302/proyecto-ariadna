@extends('layouts.admin.admin')

@section('title')
    Usuarios
@endsection

@section('content')

<div class="card card-custom">
	<div class="card-header flex-wrap py-5">
		<div class="card-title">
			<h3 class="card-label">
                Usuarios   
				<span class="d-block text-muted pt-2 font-size-sm">Lista de usuarios registrados</span>
            </h3>
        </div>
        <div class="card-toolbar">
            {!! $buttons !!}
		</div>
       
	</div>
	<div class="card-body">
        <div class="dataTables_wrapper dt-bootstrap4 no-footer">

            <table id="users" class="table table-separate table-checkable dataTable no-footer dtr-inline" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Nombre Usuario</th>
                        <th>Rol</th>
                        <th>Estado</th>
                        <th>Acción</th>
                    </tr>
                </thead>
            </table>
  
        </div>
	</div>
</div>


@include('users.create')
@include('users.update')
@include('users.status')
@include('users.password')

@endsection

@section('script')
    <script src="{{ asset('js/users.js') }}"></script>
    <script src="{{ asset('js/template/edit-user.js') }}"></script>
@endsection
